var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var pngmin = require('gulp-pngmin');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var plato = require('gulp-plato');

var paths = {
  sass: ['./scss/**/*.scss'],
  controllers: ['./www/js/*-controller.js']
};

gulp.task('default', ['sass', 'controllers']);

gulp.task('sass', function(done) {
  gulp
    .src('./scss/ionic.app.scss')
    .pipe(sass({ errLogToConsole: true }))
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass', 'controllers']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});

gulp.task('crush', function (done) {
  gulp
    .src('www/img/**/*.png')
    .pipe(pngmin())
    .pipe(gulp.dest('www/img'))
    .on('end', done);
});

gulp.task('hint', function () {
  return gulp
           .src([
             'www/js/**/*.js'
           ])
           .pipe(jshint())
           .pipe(jshint.reporter(stylish))
});

gulp.task('plato', function () {
  return gulp
           .src([
             'www/js/**/*.js'
           ])
           .pipe(plato('report', {
             jshint: {
               options: {
                 strict: false
               }
             },
             complexity: {
               trycatch: true
             }
           }))
});

gulp.task('controllers', function () {
  return gulp
           .src('./www/js/*-controller.js')
           .pipe(concat('controllers.js'))
           .pipe(gulp.dest('./www/js'));
});
