.controller('ExploreCtrl', function($scope, $state, $log, CoreService, AuthService, STAGE, $http, $location, $ionicLoading, $ionicScrollDelegate, $window) {
    $log.debug('explore controller');
    //$scope.search = {};
    //$scope.search.pro = {};
    //$scope.search.startup = ($scope.search.startup || {});
    //$scope.search.investor = {};

    $log.debug($window.localStorage.getItem('location'));

    function onAuthenticated(me) {
        $log.debug('explore: is authenticated');
        $scope.currentUser = me;
        $scope.model.currentUser = me;
        $scope.keyword = '';

        function getCurrentHandler() {
            return $location.search().current;
        }
        $scope.getCurrent = getCurrentHandler;

        function getKeywordHandler() {
            return $scope.keyword;
        }
        $scope.getKeyword = getKeywordHandler;

        function isSecondaryHandler(context) {
            return getKeywordHandler() !== context ? 'secondary' : '';
        }
        $scope.isSecondary = isSecondaryHandler;

        function chooseHandler(context) {
            $scope.keyword = context;
        }
        $scope.choose = chooseHandler;

        // set default context
        chooseHandler(getCurrentHandler() || 'startup');

        //autocomplete
        function marketsList($query) {
            return CoreService
                .tags('markets', $query);
        }
        $scope.marketsList = marketsList;

        function expertiseList($query) {
            return CoreService
                .tags('expertises', $query);
        }
        $scope.expertiseList = expertiseList;

        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {
                function onSuccess(data) {
                    $log.debug('on success handler');
                    var self = this;
                    $log.debug('entity: ' + self.entity);
                    $ionicScrollDelegate.scrollTop();
                    if (!data.err) {
                        $scope.profiles.startup = [];
                        $scope.profiles.professional = [];
                        $scope.profiles.investor = [];
                        $scope.profiles[self.entity] = data.result;
                    }
                    $ionicLoading.hide();
                }

                function onError(data, status) {
                    $log.debug('on error handler');
                    $log.debug('Something goes wrong: ' + data);
                    $ionicLoading.hide();
                }

                function searchStartup(search, keyword) {
                    $log.debug('search startup handler');
                    $log.debug(search);
                    chooseHandler(keyword);
                    $ionicLoading.show({
                        template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
                    });
                    $window.localStorage.setItem('location', search.startup.location);
                    $log.debug($window.localStorage.getItem('location'));
                    CoreService
                        .search('startup', me.id, search.startup)
                        .then(onSuccess.bind({
                            entity: 'startup'
                        }), onError);
                }
                $scope.searchStartup = searchStartup;

                function searchPro(search) {
                    $log.debug('search professional handler');
                    $log.debug(search);
                    $ionicLoading.show({
                        template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
                    });
                    localStorage.location = search.pro.location;
                    $log.debug(localStorage.location);
                    CoreService
                        .search('pro', me.id, search.pro)
                        .then(onSuccess.bind({
                            entity: 'professional'
                        }), onError);
                }
                $scope.searchPro = searchPro;

                function searchInvestor(search) {
                    $log.debug('search investor handler');
                    $log.debug(search);
                    $ionicLoading.show({
                        template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
                    });
                    localStorage.location = search.investor.location;
                    $log.debug(localStorage.location);
                    CoreService
                        .search('investor', me.id, search.investor)
                        .then(onSuccess.bind({
                            entity: 'investor'
                        }), onError);
                }
                $scope.searchInvestor = searchInvestor;
            });
    }

    function onNotAuthenticated() {
        $log.debug('explore: is not authenticated');
        $state.go('app.intro');
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(window.Hull.currentUser());
            } else {
                onNotAuthenticated();
            }
        });
    }
})
