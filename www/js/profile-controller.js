.controller('ProfileCtrl', function($scope, $state, $log, $ionicModal, AuthService, CoreService, $rootScope) {
  $scope.profile = {
      startup: {},
      professional: {},
      investor: {},
  };

  function onAuthenticated(me) {
      $log.debug('on authenticated');
      $scope.model.currentUser = me;
      $scope.currentUser = me;

      function onSuccess(data) {
          var keys = (!data.err && (data.result)) ? Object.keys(data.result) : [];
          var hasStartup = keys.indexOf('startup') > -1;
          var hasPro = keys.indexOf('professional') > -1;
          var hasInvestor = keys.indexOf('investor') > -1;

          $scope.hasStartup = hasStartup;
          $scope.hasPro = hasPro;
          $scope.hasInvestor = hasInvestor;

          $rootScope.$broadcast('profile', data.result);

          if (!data.err && (hasStartup)) {
              $scope.profile.startup = data.result.startup;
          }
          if (!data.err && (hasPro)) {
              $scope.profile.professional = data.result.professional;
          }
          if (!data.err && (hasInvestor)) {
              $scope.profile.investor = data.result.investor;
          }
      }

      function onError(data, status) {
          $log.debug('Something goes wrong: ' + data);
      }
      AuthService
          .auth(me.identities[0].provider, me)
          .success(function(data, status) {
              CoreService
                  .profile($scope.currentUser.id)
                  .then(onSuccess, onError);
          });
  }

  function onNotAuthenticated() {
      $log.debug('profile: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})
