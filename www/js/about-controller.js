.controller('AboutCtrl', function($scope, $state, $log, AuthService) {
  function onAuthenticated(me) {
      $log.debug('about: is authenticated');
  }

  function onNotAuthenticated() {
      $log.debug('settings: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})
