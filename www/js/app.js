angular
  .module('starter', [
    'ionic',
    'starter.directives',
    'starter.controllers',
    'starter.services',
    'starter.filters',
    'ngStorage',
    'ngTagsInput',
    'angularFileUpload',
  ])

  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })

      .state('app.intro', {
        url: '/intro',
        views: {
          'menuContent': {
            templateUrl: 'templates/intro.html',
            controller: 'IntroCtrl'
          }
        }
      })

      .state('app.register', {
        url: '/register/:profileAction',
        views: {
          'menuContent': {
            templateUrl: 'templates/register.html',
            controller: 'RegisterCtrl'
          }
        }
      })

      .state('app.dashboard', {
        url: '/dashboard',
        views: {
          'menuContent': {
            templateUrl: 'templates/dashboard.html',
            controller: 'DashboardCtrl'
          }
        }
      })

      .state('app.explore', {
        url: '/explore',
        views: {
          'menuContent': {
            templateUrl: 'templates/explore.html',
            controller: 'ExploreCtrl'
          }
        }
      })

      .state('app.messages', {
        url: '/messages',
        views: {
          'menuContent': {
            templateUrl: 'templates/messages.html',
            controller: 'MessagesCtrl'
          }
        }
      })

      .state('app.message', {
        cache: false,
        url: '/message/:type/:profileId/:name',
        views: {
          'menuContent': {
            templateUrl: 'templates/message.html',
            controller: 'MessageCtrl'
          }
        }
      })

      .state('app.favorites', {
        url: '/favorites',
        views: {
          'menuContent': {
            templateUrl: 'templates/favorites.html',
            controller: 'FavoritesCtrl'
          }
        }
      })

      .state('app.blog', {
        url: '/blog',
        views: {
          'menuContent': {
            templateUrl: 'templates/blog.html',
            controller: 'BlogCtrl'
          }
        }
      })

      .state('app.events', {
        url: '/events',
        views: {
          'menuContent': {
            templateUrl: 'templates/events.html',
            controller: 'EventsCtrl'
          }
        }
      })

      .state('app.post', {
        url: '/blog/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/post.html',
            controller: 'PostCtrl'
          }
        }
      })

      .state('app.profile', {
        url: '/profile',
        views: {
          'menuContent' :{
            templateUrl: 'templates/profile.html',
            controller: 'ProfileCtrl'
          }
        }
      })

      .state('app.startup', {
        url: '/profile/startup/:profileId',
        views: {
          'menuContent': {
            templateUrl: 'templates/profile-startup.html',
            controller: 'ProfileStartupCtrl'
          }
        }
      })

      .state('app.editStartup', {
        url: '/profile/edit/startup/:profileId',
        views: {
          'menuContent': {
            templateUrl: 'templates/profile-edit-startup.html',
            controller: 'ProfileEditStartupCtrl'
          }
        }
      })

      .state('app.startupSearchMember', {
        url: "/profile/search/member/startup/:profileId",
        views: {
          'menuContent': {
            templateUrl: "templates/startup-search-member.html",
            controller: 'StartupSearchMemberCtrl'
          }
        }
      })

      .state('app.professional', {
        url: '/profile/professional/:profileId',
        views: {
          'menuContent': {
            templateUrl: 'templates/profile-professional.html',
            controller: 'ProfileProfessionalCtrl'
          }
        }
      })

      .state('app.editProfessional', {
        url: '/profile/edit/professional/:profileId',
        views: {
          'menuContent': {
            templateUrl: 'templates/profile-edit-pro.html',
            controller: 'ProfileEditProfessionalCtrl'
          }
        }
      })

      .state('app.investor', {
        url: '/profile/:profileType/:profileId',
        views: {
          'menuContent': {
            templateUrl: 'templates/profile-investor.html',
            controller: 'ProfileInvestorCtrl'
          }
        }
      })

      .state('app.editInvestor', {
        url: '/profile/edit/investor/:profileId',
        views: {
          'menuContent': {
            templateUrl: 'templates/profile-edit-investor.html',
            controller: 'ProfileEditInvestorCtrl'
          }
        }
      })
      
      .state('app.share', {
        url: '/share',
        views: {
          'menuContent': {
            templateUrl: 'templates/share.html',
            controller: 'ShareCtrl'
          }
        }
      })

      .state('app.about', {
        url: '/about',
        views: {
          'menuContent': {
            templateUrl: 'templates/about.html',
            controller: 'AboutCtrl'
          }
        }
      })

      .state('app.contact', {
        url: '/contact',
        views: {
          'menuContent': {
            templateUrl: 'templates/contact.html',
            controller: 'ContactCtrl'
          }
        }
      })

      .state('app.auto', {
        url: '/auto',
        views: {
          'menuContent': {
            templateUrl: 'templates/autocomplete.html',
            controller: 'AutocompleteCtrl'
          }
        }
      })
      
      .state('app.logout', {
        url: '/logout',
        views: {
          'menuContent': {
            templateUrl: 'templates/logout.html',
            controller: 'LogoutCtrl'
          }
        }
      });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/intro');
  })

  .run(function($rootScope, $ionicPlatform, $state) {
    window.Hull.on('hull.init', function(hull, me, app, org){
      $ionicPlatform.ready(function() {
        $rootScope.currentUser = me;
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if(window.cordova && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if(window.StatusBar) {
          // org.apache.cordova.statusbar required
          StatusBar.styleDefault();
        }
      });
    });

    $rootScope.goto_state = function(state_uri, params) {
      params = params || {};
      $state.go(state_uri, params);
    }
  });

  function onReady() {
    angular.bootstrap(document, ['starter']);
  }

  if (document.ondeviceready) {
    angular.element(document).on('deviceready', onReady);
  } else {
    angular.element(document).ready(onReady);
  }
