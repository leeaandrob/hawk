angular
  .module('starter.constant', [])

  .constant('PROD', '45.55.173.180')

  .constant('STAGE', '45.55.178.207')

  .constant('DEV', window.location.host.replace(/\:[0-9]+/, ':3000'))

  .run(function ($log) {
    $log.debug('App constant running!');
  });
