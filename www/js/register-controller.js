.controller('RegisterCtrl', function($scope, $state, $ionicModal, $ionicPopup, AuthService, CoreService, STAGE, $http, $log, $upload, $stateParams) {
    $scope.data = {
        "countries": [],
        "cities": [],
        type: ""
    };

    $scope.percentage = 0;
    $scope.profile = {
        pro: {
            avatar: ''
        },
        startup: {
            avatar: '',
            team: [],
        },
        investor: {
            avatar: ''
        },
        tmp: {}
    };
    $scope.profiles = [{
        title: 'Startup',
        name: 'startup',
        full_name: 'startup',
        icon: 'img/startup-circle-icon.png',
        checked: false
    }, {
        title: 'Pro',
        name: 'pro',
        full_name: 'professional',
        icon: 'img/pro-circle-icon.png',
        checked: false
    }, {
        title: 'Investidor',
        name: 'investor',
        full_name: 'investor',
        icon: 'img/investor-circle-icon.png',
        checked: false
    }, ];

    $scope.profileAction = $stateParams.profileAction;

    function saveHandler(entity) {
        var hasAvatar = $scope.profile[entity].avatar;
        if (!hasAvatar && ($scope.model.currentUser.picture.indexOf('graph.facebook') > -1)) {
            $scope.profile[entity].avatar = $scope.model.currentUser.picture + '?type=large';
        } else if (!hasAvatar) {
            $scope.profile[entity].avatar = $scope.model.currentUser.picture;
        }

        function onSuccess(data) {
            if (!data.err) {
                if (entity == 'startup') {
                    $scope.profile.pro.site = $scope.profile['startup'].site;
                    $scope.profile.pro.city = $scope.profile['startup'].city;
                    $scope.profile.pro.country = $scope.profile['startup'].country;

                    $scope.modal.remove();
                    $scope.enableState(1, 'pro');
                } else {
                    $state.go('app.dashboard');
                    $scope.modal.remove();
                }
            }
        }

        function onError(data, status) {
            $log.debug('Something goes wrong: ' + data);
        }
        if ($scope.profile.isValid) {
            CoreService
                .create(entity, $scope.profile[entity], $scope.currentUser.id)
                .then(onSuccess, onError);
        } else if ($scope.profile.isInvalid) {
            $log.debug('Form invalid!');
        }
    }
    $scope.save = saveHandler;

    function setPercentageHandler(percentage) {
        $scope.percentage = percentage;
    }
    $scope.setPercentage = setPercentageHandler;

    function getPercentageHandler() {
        return $scope.percentage;
    }
    $scope.getPercentage = getPercentageHandler;

    function setStartupAvatarHandler(avatar) {
        $scope.profile.startup.avatar = avatar;
    }
    $scope.setStartupAvatar = setStartupAvatarHandler;

    function setProAvatarHandler(avatar) {
        $scope.profile.pro.avatar = avatar;
    }
    $scope.setProAvatar = setProAvatarHandler;

    function setInvestorAvatarHandler(avatar) {
        $scope.profile.investor.avatar = avatar;
    }
    $scope.setInvestorAvatar = setInvestorAvatarHandler;

    function onFileProgress(e) {
        var percentage = parseInt(100.0 * e.loaded / e.total);
        $log.debug('precent: ' + percentage + '%');
        setPercentageHandler(percentage);

        document.getElementById('progressBar').style.display = 'block';

        $scope.barProgress = percentage;
    }

    function onFileSuccess(data) {
        document.getElementById('progressBar').style.display = 'none';
        $log.debug('on file success');
        $log.debug(data);
        $log.debug(currentType);
        var self = this;
        var currentType = self.currentType;
        var avatarURL = 'http://' + STAGE + '/api/avatar/' + data.result._id;

        if (currentType === 'startup') {
            setStartupAvatarHandler(avatarURL);
            setProAvatarHandler('');
            setInvestorAvatarHandler('');
        } else if (currentType === 'pro') {
            setProAvatarHandler(avatarURL);
            setStartupAvatarHandler('');
            setInvestorAvatarHandler('');
        } else if (currentType === 'investor') {
            setInvestorAvatarHandler(avatarURL);
            setStartupAvatarHandler('');
            setProAvatarHandler('');
        }
        setPercentageHandler(0);
    }

    function onFileError() {
        $log.debug('on file error');
    }

    function onFileSelect($files, currentType) {
        $log.debug('on file select');
        $log.debug($files);
        for (var i = 0; i < $files.length; i++) {
            var file = $files[i];
            $scope
                .upload =
                $upload
                .upload({
                    url: 'http://' + STAGE + '/api/avatar',
                    file: file,
                    fileFormDataName: 'image'
                })
                .progress(onFileProgress)
                .success(onFileSuccess.bind({
                    currentType: currentType
                }))
                .error(onFileError);
        }
    }
    $scope.onFileSelect = onFileSelect;

    function enableState(id, name) {
        $scope.data.type = name;
        var templateLocation = 'templates/settings-' + name + '.html';

        $scope.id = id;

        $ionicModal.fromTemplateUrl(templateLocation, {
            scope: $scope,
            animation: 'slide-in-left'
        }).then(function(modal) {
            $scope.modal = modal;
            modal.show();
        });
    }
    $scope.enableState = enableState;

    function goToDashboard() {
        $state.go('app.dashboard');
    }
    $scope.goToDashboard = goToDashboard;

    function isMemberValid(member) {
        return member.lookingForExpertise.length > 0 && ( member.offerEquity || member.offerCash );
    }

    function addToTeam(member) {
        if( isMemberValid(member) ) {
            $scope.profile.startup.team.push(member);
            $scope.profile.tmp = {};
            $('#offerEquity').prop('checked', false);
            $('#offerCash').prop('checked', false);
        }
    }
    $scope.addToTeam = addToTeam;

    function removeTagFromTeam(parentIndex, i) {
        $scope.profile.startup.team[parentIndex].lookingForExpertise.splice(i, 1);
        if (!$scope.profile.startup.team[parentIndex].lookingForExpertise.length) {
            removeFromTeam(parentIndex);
        }
    }
    $scope.removeTagFromTeam = removeTagFromTeam;

    function removeFromTeam(i) {
        $scope.profile.startup.team.splice(i, 1);
    }

    function onAuthenticated(me) {
        $log.debug('profile: is authenticated');
        $scope.model.currentUser = me;
        $scope.currentUser = me;
        $log.debug(me);

        function onSuccess(data) {
            $log.debug(data);
            var keys = (!data.err && (data.result)) ? Object.keys(data.result) : [];
            var hasStartup = keys.indexOf('startup') > -1;
            var hasPro = keys.indexOf('professional') > -1;
            var hasInvestor = keys.indexOf('investor') > -1;
            var hasOneEntity = (keys ? (hasStartup || (hasPro || (hasInvestor))) : null);

            $scope.isProfileAuthenticated = true;

            if (!data.err && (hasOneEntity)) {

                if ($scope.profileAction == "add") {

                    $scope.profileKeys = Object.keys(data.result);

                } else {
                    if (hasStartup) {

                        CoreService
                            .list('pro', me.id)
                            .then(
                                function(data) {
                                    if (data.result.length == 0) {
                                        $scope.enableState(1, 'pro');
                                    } else {
                                        $state.go('app.dashboard');
                                    }
                                },
                                onError);

                    } else {
                        $state.go('app.dashboard');
                    }
                }
            }
        }

        function onError(data, status) {
            $log.debug('Something goes wrong: ' + data);
        }

        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {
                CoreService
                    .hasEntity(me.id)
                    .then(onSuccess, onError);
            });
    }

    //country autocompelte
    function searchCountry() {
        if (
                    $scope.profile[$scope.data.type].country != undefined 
                &&  $scope.profile[$scope.data.type].country.length >= 3
            ) {
            var countries = CoreService
                .tags('countries', $scope.profile[$scope.data.type].country);
            countries.then(function(c) {
                $scope.data.countries = c;
            });
        }
    }
    $scope.searchCountry = searchCountry;

    function onClickACCountry(i) {
        $scope.profile[$scope.data.type].country = $scope.data.countries[i];
        $scope.data.countries = [];
    }
    $scope.onClickACCountry = onClickACCountry;

    //city autocomplete
    var searchCityTimer = null;
    function searchCity() {
        $scope.data.cities = [];
        if (
                $scope.profile[$scope.data.type].city != undefined 
            &&  $scope.profile[$scope.data.type].city.length >= 3
        ) {
            if(searchCityTimer != null) clearTimeout(searchCityTimer);
            searchCityTimer = setTimeout(function() {
                console.log("here");
                console.log($scope.profile[$scope.data.type].city);
                var cities = CoreService
                    .tags('cities', $scope.profile[$scope.data.type].city);
                cities.then(function(c) {
                    $scope.data.cities = c;
                });
            }, 500);                
        }
    }   
    $scope.searchCity = searchCity;

    function onClickACCity(i) {
        $scope.profile[$scope.data.type].city = $scope.data.cities[i];
        $scope.data.cities = [];
    }
    $scope.onClickACCity = onClickACCity;

    //autocomplete
    function marketsList($query) {
        return CoreService
            .tags('markets', $query);
    }
    $scope.marketsList = marketsList;

    function expertiseList($query) {
        return CoreService
            .tags('expertises', $query);
    }
    $scope.expertiseList = expertiseList;

    function onNotAuthenticated() {
        $log.debug('profile: is not authenticated');
        $state.go('app.intro');
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(me);
            } else {
                onNotAuthenticated();
            }
        });

        window.Hull.on('hull.auth.login', function(me) {
            if (me) {
                onAuthenticated(me);
            } else {
                onNotAuthenticated();
            }
        });
    }
})
