angular
  .module('starter.directives', [])

  .directive('introMenu', function () {
    return {
      templateUrl: 'templates/intro-menu.html',
    };
  })

  .directive('contextMenu', function () {
    return {
      templateUrl: 'templates/context-menu.html',
    };
  })

  .directive('menuTools', function () {
    return {
      templateUrl: 'templates/menu-tools.html',
    };
  })

  .directive('searchingFor', function () {
    return {
      templateUrl: 'templates/searching-for.html',
    };
  })

  .directive('expertisesSetting', function () {
    return {
      templateUrl: 'templates/expertises-setting.html',
    };
  })

  .directive('cardStartup', function () {
    return {
      templateUrl: 'templates/card-startup.html',
    };
  })

  .directive('cardSecondaryStartup', function () {
    return {
      templateUrl: 'templates/card-secondary-startup.html',
    };
  })

  .directive('cardExploreStartup', function () {
    return {
      templateUrl: 'templates/card-explore-startup.html',
    };
  })

  .directive('cardPro', function () {
    return {
      templateUrl: 'templates/card-pro.html' 
    };
  })

  .directive('cardSecondaryPro', function () {
    return {
      templateUrl: 'templates/card-secondary-pro.html',
    };
  })

  .directive('cardTeamPro', function () {
    return {
      templateUrl: 'templates/card-team-pro.html',
    };
  })

  .directive('cardExplorePro', function () {
    return {
      templateUrl: 'templates/card-explore-pro.html',
    };
  })

  .directive('cardInvestor', function () {
    return {
      templateUrl: 'templates/card-investor.html',
    };
  })

  .directive('cardSecondaryInvestor', function () {
    return {
      templateUrl: 'templates/card-secondary-investor.html',
    };
  })

  .directive('cardExploreInvestor', function () {
    return {
      templateUrl: 'templates/card-explore-investor.html',
    };
  })

  .directive('profileActions', function (Profile) {
    return {
      restrict: 'A',
      templateUrl: 'templates/profile-actions.html',
      link: function(scope, element, attrs) {
        scope.is_in_whitelist = function() {
          var is_in = false;

          if(scope.profile.icognitoWhiteList) {
            var profile_logged = Profile.get_logged();
            if('investor' in profile_logged)
              is_in = is_in || scope.profile.icognitoWhiteList.indexOf(profile_logged.investor.profile_id) > -1;
            if('startup' in profile_logged)
              is_in = is_in || scope.profile.icognitoWhiteList.indexOf(profile_logged.startup.profile_id) > -1;
            if('pro' in profile_logged)
              is_in = is_in || scope.profile.icognitoWhiteList.indexOf(profile_logged.pro.profile_id) > -1;
          }

          return is_in;
        }

        scope.is_mensagem_disabled = function() {
          return (scope.profile.stealth) || (scope.profile.icognito && !scope.is_in_whitelist());
        }
      }
    };
  })

  .directive('recommendations', function () {
    return {
      templateUrl: 'templates/recommendations.html',
    };
  })

  .directive('backImg', function(){
      return function(scope, element, attrs){
          attrs.$observe('backImg', function(value) {
              element.css({
                  'background-image': 'url(' + value +')'
              });
          });
      };
  })

  .run(function ($log) {
    $log.debug('directives running!');
  });
