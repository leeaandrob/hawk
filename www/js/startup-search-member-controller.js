.controller('StartupSearchMemberCtrl', function($scope, $log, $state, $ionicPopup, AuthService, CoreService, StartupManager) {
    function init() {
        $scope.profiles = {};

        $scope.doSearch = function(query_keyword) {
            console.log(query_keyword);
            CoreService.search('pro', $scope.currentUser.id, {
                keyword: query_keyword
            }).then(
                function(data) {
                    $scope.profiles.professional = data.result;
                }
            );

            CoreService.search('investor', $scope.currentUser.id, {
                keyword: query_keyword
            }).then(
                function(data) {
                    $scope.profiles.investor = data.result;
                }
            );
        }

        $scope.inviteMember = function(profile, type) {
            var inviteMemberPopup = $ionicPopup.show({
                title: 'Convidar ' + profile.name,
                template: 'Você deseja convidar o ' + profile.name + ' para sua Startup?',
                buttons: [{
                    text: 'Cancelar'
                }, {
                    text: '<b>Convidar</b>',
                    type: 'button-positive',
                    onTap: function(e) {

                        StartupManager.addMember($scope.profile.startup, profile, type, $scope.currentUser.id).then(
                            function(result) {
                                console.log('result');
                                console.log(result);

                                inviteMemberPopup.close();

                                var alertPopup = $ionicPopup.alert({
                                    title: profile.name + ' foi convidado',
                                    template: 'Aguarde a confirmação do novo membro para sua Startup',
                                });

                            },
                            function(error) {
                                console.log('error');
                                console.log(error);
                            }
                        );

                    }
                }]
            });
        };

    }

    function onAuthSuccess(data, status) {
        CoreService.profile($scope.currentUser.id).then(
            function(data) {
                var keys = (!data.err && (data.result)) ? Object.keys(data.result) : [];
                var hasStartup = keys.indexOf('startup') > -1;

                if (!data.err && (hasStartup)) {
                    $scope.profile = {};
                    $scope.profile.startup = data.result.startup;

                    init();
                }
            }
        );
    }

    function onAuthenticated(me) {
        $log.debug('startup: is authenticated');
        $scope.model.currentUser = me;
        $scope.currentUser = me;

        AuthService
            .auth(me.identities[0].provider, me)
            .success(onAuthSuccess);
    }

    function onNotAuthenticated() {
        $log.debug('startup: is not authenticated');
        $state.go('app.intro');
    }

    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(window.Hull.currentUser());
            } else {
                onNotAuthenticated();
            }
        });
    }
})
