.controller('AutocompleteCtrl', ['$scope', 'AutoCompleteService', function($scope, AutoCompleteService) {
  $scope.myTitle = 'Auto Complete';

  $scope.data = {
      "airlines": [],
      "search": ''
  };

  $scope.search = function() {
      AutoCompleteService.searchAirlines($scope.data.search).then(
          function(matches) {
              $scope.data.airlines = matches;
          }
      )
  };
}])
