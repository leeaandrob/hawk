.controller('BlogCtrl', function($scope, $log, $state, CoreService, AuthService) {
    function onAuthenticated(me) {
        $log.debug('blog: is authenticated');
        $scope.model.currentUser = me;
        $scope.currentUser = me;

        function onSuccess(data) {
            $log.debug('on success handler');
            $scope.posts = data.result;
        }

        function onError(data, status) {
            $log.debug('on error handler');
            $log.debug('Something goes wrong: ' + data);
        }
        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {
                CoreService
                    .blogPosts(me.id)
                    .then(onSuccess, onError);
            });
    }

    function onNotAuthenticated() {
        $log.debug('favorites: is not authenticated');
        $state.go('app.intro');
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(window.Hull.currentUser());
            } else {
                onNotAuthenticated();
            }
        });
    }
})

.controller('EventsCtrl', function($scope, $log, $state, CoreService, AuthService) {
    function onAuthenticated(me) {
        $log.debug('blog: is authenticated');
        $scope.model.currentUser = me;
        $scope.currentUser = me;

        function onSuccess(data) {
            $log.debug('on success handler');

            function filterPosts(post) {
                if (post.tags.indexOf('evento') > -1) {
                    return post;
                }
            }
            $scope.posts = data.result.filter(filterPosts);
        }

        function onError(data, status) {
            $log.debug('on error handler');
            $log.debug('Something goes wrong: ' + data);
        }
        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {
                CoreService
                    .blogPosts(me.id)
                    .then(onSuccess, onError);
            });
    }

    function onNotAuthenticated() {
        $log.debug('favorites: is not authenticated');
        $state.go('app.intro');
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(window.Hull.currentUser());
            } else {
                onNotAuthenticated();
            }
        });
    }
})

.controller('PostCtrl', function($scope, $log, $state, CoreService, AuthService) {
    function onAuthenticated(me) {
        $log.debug('post: is authenticated');
        $scope.model.currentUser = me;
        $scope.currentUser = me;

        function onSuccess(data) {
            $log.debug('on success handler');
            $log.debug('post id: ' + $state.params.id);

            function filterPost(post) {
                $log.debug('post id in list: ' + post.id);
                if (post.id.toString() === $state.params.id.toString()) {
                    $log.debug('post matched!');
                    return post;
                }
            }
            $scope.post = data.result.filter(filterPost)[0];
        }

        function onError(data, status) {
            $log.debug('on error handler');
            $log.debug('Something goes wrong: ' + data);
        }
        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {
                CoreService
                    .blogPosts(me.id)
                    .then(onSuccess, onError);
            });
    }

    function onNotAuthenticated() {
        $log.debug('favorites: is not authenticated');
        $state.go('app.intro');
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(window.Hull.currentUser());
            } else {
                onNotAuthenticated();
            }
        });
    }
})
