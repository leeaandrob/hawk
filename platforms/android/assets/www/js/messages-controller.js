.controller('MessagesCtrl', function($scope, $log, $state, AuthService, CoreService, $MessageManager, StartupManager) {
  function onAuthSuccess(data, status) {
      $MessageManager.getInboxMessages($scope.currentUser.id).then(
          function(messages) {
              $scope.messages = messages;
          }
      );

      $scope.openStartupProfile = function(startup) {
          console.log(startup);
          $state.go('app.startup', {
              profileId: startup._id
          });
      }
  }

  function onAuthenticated(me) {
      $log.debug('messages: is authenticated');
      $scope.model.currentUser = me;
      $scope.currentUser = me;

      AuthService
          .auth(me.identities[0].provider, me)
          .success(onAuthSuccess);
  }

  function onNotAuthenticated() {
      $log.debug('messages: is not authenticated');
      $state.go('app.intro');
  }

  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})
