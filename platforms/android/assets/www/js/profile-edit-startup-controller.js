.controller('ProfileEditStartupCtrl', function($scope, $state, $log, CoreService, AuthService, $location, $upload, STAGE, $ionicNavBarDelegate) {
  $scope.profile = {};
  $scope.profile.startup = {};
  $scope.profile.tmp = {};
  $scope.profile.team = [];

  function onAuthenticated(me) {
      $log.debug('profile-edit-startup: is authenticated');
      $scope.currentUser = me;
      $scope.model.currentUser = me;

      function isMemberValid(member) {
          return member.lookingForExpertise.length > 0 && ( member.offerEquity || member.offerCash );
      }

      function addToTeam(member) {
          if( isMemberValid(member) ) {
              $scope.profile.startup.team.push(member);
              $scope.profile.tmp = {};
              $('#offerEquity').prop('checked', false);
              $('#offerCash').prop('checked', false);
          }
      }
      $scope.addToTeam = addToTeam;

      function removeTagFromTeam(parentIndex, i) {
          $scope.profile.startup.team[parentIndex].lookingForExpertise.splice(i, 1);
          if (!$scope.profile.startup.team[parentIndex].lookingForExpertise.length) {
              removeFromTeam(parentIndex);
          }
      }
      $scope.removeTagFromTeam = removeTagFromTeam;

      function removeFromTeam(i) {
          $scope.profile.startup.team.splice(i, 1);
      }

      function onReadOneSuccess(data, status) {
          $log.debug('on read one success');
          $('#lookingForPros').prop('checked', data.result.lookingForPros);
          $('#lookingForInvestor').prop('checked', data.result.lookingForInvestor);
          $scope.profile.team = data.result.team;
          console.log(data.result);
          $scope.profile.startup = data.result;
      }

      function onReadOneError(data, status) {
          $log.debug('on read one error');
          $log.debug('Something goes wrong: ' + data);
      }

      $scope.goBack = function() {
          $ionicNavBarDelegate.back();
      };

      function editHandler(entity) {
          function onSuccess(data) {
              if (!data.err && data.status) {
                  $scope.goBack();
              }
          }

          function onError(data, status) {
              $log.debug('Something goes wrong: ' + data);
          }

          CoreService
              .edit(entity, {
                  avatar: $scope.profile.startup.avatar,
                  name: $scope.profile.startup.name,
                  site: $scope.profile.startup.site,
                  country: $scope.profile.startup.country,
                  city: $scope.profile.startup.city,
                  tagline: $scope.profile.startup.tagline,
                  description: $scope.profile.startup.description,
                  market: $scope.profile.startup.market,
                  developmentStage: $scope.profile.startup.developmentStage,
                  lookingForPros: $scope.profile.startup.lookingForPros,
                  team: $scope.profile.startup.team,
                  lookingForInvestor: $scope.profile.startup.lookingForInvestor
              }, $scope.currentUser.id)
              .then(onSuccess, onError);
      }
      $scope.edit = editHandler;

      function setPercentageHandler(percentage) {
          $scope.percentage = percentage;
      }
      $scope.setPercentage = setPercentageHandler;

      function getPercentageHandler() {
          return $scope.percentage;
      }
      $scope.getPercentage = getPercentageHandler;

      function setStartupAvatarHandler(avatar) {
          $scope.profile.startup.avatar = avatar;
      }
      $scope.setStartupAvatar = setStartupAvatarHandler;

      function onFileProgress(e) {
          var percentage = parseInt(100.0 * e.loaded / e.total);
          $log.debug('precent: ' + percentage + '%');
          setPercentageHandler(percentage);

          document.getElementById('progressBar').style.display = 'block';

          $scope.barProgress = percentage;
      }

      function onFileSuccess(data) {
          document.getElementById('progressBar').style.display = 'none';
          $log.debug('on file success');
          $log.debug(data);
          var avatarURL = 'http://' + STAGE + '/api/avatar/' + data.result._id;

          setStartupAvatarHandler(avatarURL);
          setPercentageHandler(0);
      }

      function onFileError() {
          $log.debug('on file error');
      }

      function onFileSelect($files, currentType) {
          $log.debug('on file select');
          $log.debug($files);
          for (var i = 0; i < $files.length; i++) {
              var file = $files[i];
              $scope
                  .upload =
                  $upload
                  .upload({
                      url: 'http://' + STAGE + '/api/avatar',
                      file: file,
                      fileFormDataName: 'image'
                  })
                  .progress(onFileProgress)
                  .success(onFileSuccess)
                  .error(onFileError);
          }
      }
      $scope.onFileSelect = onFileSelect;

      $scope.data = {
          "countries": [],
          "cities": []
      };
      //country autocompelte
      function searchCountry() {
          if (
                  $scope.profile.startup.country != undefined 
              &&  $scope.profile.startup.country.length >= 3
          ) {
              var countries = CoreService
                  .tags('countries', $scope.profile.startup.country);
              countries.then(function(c) {
                  $scope.data.countries = c;
              });
          }
      }
      $scope.searchCountry = searchCountry;

      function onClickACCountry(i) {
          $scope.profile.startup.country = $scope.data.countries[i];
          $scope.data.countries = [];
      }
      $scope.onClickACCountry = onClickACCountry;

      //city autocomplete
      var searchCityTimer = null;
      function searchCity() {
          $scope.data.cities = [];
          if (
                  $scope.profile.startup.city != undefined 
              &&  $scope.profile.startup.city.length >= 3
          ) {
              if(searchCityTimer != null) clearTimeout(searchCityTimer);
              searchCityTimer = setTimeout(function() {
                  console.log("here");
                  console.log($scope.profile.startup.city);
                  var cities = CoreService
                      .tags('cities', $scope.profile.startup.city);
                  cities.then(function(c) {
                      $scope.data.cities = c;
                  });
              }, 500);                
          }
      }
      $scope.searchCity = searchCity;

      function onClickACCity(i) {
          $scope.profile.startup.city = $scope.data.cities[i];
          $scope.data.cities = [];
      }
      $scope.onClickACCity = onClickACCity;

      //autocomplete
      function marketsList($query) {
          return CoreService
              .tags('markets', $query);
      }
      $scope.marketsList = marketsList;

      function expertiseList($query) {
          return CoreService
              .tags('expertises', $query);
      }
      $scope.expertiseList = expertiseList;

      AuthService
          .auth(me.identities[0].provider, me)
          .success(function(data, status) {
              CoreService
                  .readOne('startup', me.id, $state.params.profileId)
                  .then(onReadOneSuccess, onReadOneError);
          });
  }

  function onNotAuthenticated() {
      $log.debug('profile-edit-startup: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})
