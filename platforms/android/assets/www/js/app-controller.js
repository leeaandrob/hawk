.controller('AppCtrl', function($scope, $log, $state, AuthService, $ionicLoading, $timeout, $window) {
    $scope.hasFinished = false;
    $scope.fixSair = $(window).height() - 243 + 'px';
    $scope.model = {
        currentUser: {}
    };

    $scope.login = AuthService.login;
    $scope.browserEnabled = /Mobile|Chrome/.test($window.navigator.userAgent);

    function onAuthenticated(me) {
        $scope.me = me;
        $log.debug('app: authenticated');
        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {
                $state.go('app.register');
            });
    }

    function onNotAuthenticated() {
        $log.debug('app: is not authenticated');
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(window.Hull.currentUser());
            } else {
                onNotAuthenticated();
            }
        });

        window.Hull.on('hull.auth.login', function(me) {
            if (me) {
                onAuthenticated(me);
            } else {
                onNotAuthenticated();
            }
        });
    }

    $ionicLoading.show({
        template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
    });

    $timeout(function() {
        $ionicLoading.hide();
        $timeout(function() {
            $scope.hasFinished = true;
        }, 300);
    }, 5000);
})
