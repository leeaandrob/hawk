.controller('FavoritesCtrl', function($scope, $log, $state, CoreService, AuthService) {
  $scope.favorites = {
      professional: [],
      startup: [],
      investor: [],
  };

  function onAuthenticated(me) {
      $log.debug('favorites: is authenticated');
      $scope.model.currentUser = me;
      $scope.currentUser = me;

      function profileMapHandler(item) {
        $log.debug(item);
        var type = (item.type === 'pro' ? 'professional' : item.type);
        $scope.favorites[type].push(item);
      }

      function onSuccess(data) {
          $log.debug('on success handler');
          $log.debug('data');
          $log.debug(data.result);
          data.result.map(profileMapHandler);
      }

      function onError(data, status) {
          $log.debug('on error handler');
          $log.debug('Something goes wrong: ' + data);
      }
      AuthService
          .auth(me.identities[0].provider, me)
          .success(function(data, status) {
              CoreService
                  .favoriteList(me.id)
                  .then(onSuccess, onError);
          });
  }

  function onNotAuthenticated() {
      $log.debug('favorites: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})
