.controller('DashboardCtrl', function($scope, $rootScope, $log, $state, AuthService, CoreService, $ionicLoading, $timeout, $ionicScrollDelegate, $ionicModal, Profile, $localStorage, $MessageManager, StartupManager) {
    

    $scope.hasFinished = false;
    $scope.tab = {
        counter: 0,
        selected: ''
    };
    $scope.profiles = {
        startup: [],
        professional: [],
        investor: []
    };
    $scope.modal = {
        hide: function() {}
    };
    $scope.searched = false;
    $scope.lastSearch = '';
    $scope.search = {
      startup: $localStorage.startup || {},
      pro: $localStorage.pro || {},
      investor: $localStorage.investor || {}
    };

    $scope.handleEnterKey = function(keyEvent) {
        if (keyEvent.keyCode === 13){
            keyEvent.preventDefault();
            return false;
        }
    };

    CoreService.profile($scope.currentUser.id).then(
        function(data) {
            var keys = (!data.err && (data.result)) ? Object.keys(data.result) : [];
            var hasPro = keys.indexOf('professional') > -1;
            var hasInvestor = keys.indexOf('investor') > -1;

            if (!data.err && (hasPro)) {
                $scope.model.professional = data.result.professional;
                console.log($scope.model.professional)

                StartupManager.getInvites($scope.model.professional, 'pro', $scope.currentUser.id).then(
                    function(startup_invites) {
                        $scope.startup_invites_pro = startup_invites;
                    }
                );
            } else if (!data.err && (hasInvestor)) {
                $scope.model.investor = data.result.investor;
                console.log($scope.model.investor);

                StartupManager.getInvites($scope.model.investor, 'investor', $scope.currentUser.id).then(
                    function(startup_invites) {
                        $scope.startup_invites_investor = startup_invites;
                    }
                );
            }

        }
    );

    $scope.confirmMember = function(type, startup_invite, startup_invites_index) {
        if (type === 'pro') {
          StartupManager.confirmMember($scope.model.professional, type, startup_invite, $scope.currentUser.id).then(
            function(result) {
                $scope.startup_invites_pro.pop(startup_invites_index);
                console.log(result);
            },
            function(error) {
                console.log(error);
            }
          );
        } else if (type === 'investor') {
         StartupManager.confirmMember($scope.model.investor, type, startup_invite, $scope.currentUser.id).then(
            function(result) {
                $scope.startup_invites_investor.pop(startup_invites_index);
                console.log(result);
            },
            function(error) {
                console.log(error);
            }
          ); 
        }
    }

    $scope.refuseMember = function(type, startup_invite, startup_invites_index) {
        StartupManager.refuseMember($scope.model.professional, type, startup_invite, $scope.currentUser.id).then(
            function(result) {
                if (type == 'pro') {
                    $scope.startup_invites_pro.pop(startup_invites_index);
                } else {
                    $scope.startup_invites_investor.pop(startup_invites_index);
                }
                console.log(result);
            },
            function(error) {
                console.log(error);
            }
        );
    }

    function onAuthenticated(me) {
        $log.debug('dashboard: is authenticated');
        $scope.keyword = setCurrentHandler();

        $MessageManager.getInboxMessages($scope.currentUser.id || me.id).then(
            function(messages) {
                $scope.messagesCounter = messages.length;
            }
        );

        function onSwipeLeft() {
            $log.debug('swipe left');
            $scope.tab.counter += 1;
            if ($scope.tab.counter === 1) {
                $scope.choose('pro');
            } else if ($scope.tab.counter === 2) {
                $scope.choose('investor');
            } else if ($scope.tab.counter === 3) {
                $scope.choose('startup');
            } else {
                $scope.choose('startup');
                $scope.tab.counter = 0;
            }
        }
        $scope.onSwipeLeft = onSwipeLeft;

        function onSearchSuccess(data) {
            $log.debug('on success handler');
            var self = this;
            $log.debug('entity: ' + self.entity);
            $ionicScrollDelegate.scrollTop();
            if (!data.err) {
                $scope.choose(self.entity === 'professional' ? 'pro' : self.entity);
                $scope.profiles[self.entity] = data.result;
                $scope.searched = true;
                $scope.lastSearch = self.entity;
                $scope.modal.hide();
            }
            $ionicLoading.hide();
        }

        function cleanSearch() {
            $scope.searched = false;

            $ionicLoading.show({
                template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
            });

            if ($scope.lastSearch === 'startup') {
                CoreService
                    .list('startup', me.id)
                    .then(onSuccess.bind({
                        entity: 'startup'
                    }), onError);
            } else if ($scope.lastSearch === 'professional') {
                CoreService
                    .list('pro', me.id)
                    .then(onSuccess.bind({
                        entity: 'professional'
                    }), onError);
            } else if ($scope.lastSearch === 'investor') {
                CoreService
                    .list('investor', me.id)
                    .then(onSuccess.bind({
                        entity: 'investor'
                    }), onError);
            }
        }
        $scope.cleanSearch = cleanSearch;

        function onSearchError(data, status) {
            $log.debug('on error handler');
            $log.debug('Something goes wrong: ' + data);
            $ionicLoading.hide();
        }

        function searchStartup() {
            $log.debug('search startup handler');
            $log.debug($scope.search);
            $localStorage.startup = $scope.search.startup;
            $log.debug($localStorage.startup);
            $ionicLoading.show({
                template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
            });
            CoreService
                .search('startup', me.id, $scope.search.startup)
                .then(onSearchSuccess.bind({
                    entity: 'startup'
                }), onSearchError);
        }
        $scope.searchStartup = searchStartup;

        function searchPro() {
            $log.debug('search professional handler');
            $localStorage.pro = $scope.search.pro;
            $log.debug($localStorage.pro);
            $ionicLoading.show({
                template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
            });
            CoreService
                .search('pro', me.id, $scope.search.pro)
                .then(onSearchSuccess.bind({
                    entity: 'professional'
                }), onSearchError);
        }
        $scope.searchPro = searchPro;

        function searchInvestor() {
            $log.debug('search investor handler');
            $localStorage.investor = $scope.search.investor;
            $ionicLoading.show({
                template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
            });
            CoreService
                .search('investor', me.id, $scope.search.investor)
                .then(onSearchSuccess.bind({
                    entity: 'investor'
                }), onSearchError);
        }
        $scope.searchInvestor = searchInvestor;

        function enableState() {
            $ionicModal.fromTemplateUrl('templates/explore.html', {
                scope: $scope,
                animation: 'slide-in-left'
            }).then(function(modal) {
                $scope.modal = modal;
                modal.show();
            });
        }
        $scope.enableState = enableState;

        function setCurrentHandler() {
            var out = 'startup';
            var hasStartup = $scope.profiles.startup.length > 0;
            var hasPro = $scope.profiles.professional.length > 0;
            var hasInvestor = $scope.profiles.investor.length > 0;

            if (hasStartup) {
                out = 'startup';
            } else if (hasPro) {
                out = 'pro';
            } else if (hasInvestor) {
                out = 'investor';
            }

            return out;
        }

        function isCurrentHandler(current) {
            var out = false;

            if (hasKeywordHandler()) {
                out = getKeywordHandler() === current;
            }

            return out;
        }
        $scope.isCurrent = isCurrentHandler;

        function onProfileSuccess(data) {
            $log.debug(data);
            var keys = (!data.err && (data.result)) ? Object.keys(data.result) : [];
            var hasStartup = keys.indexOf('startup') > -1;
            var hasPro = keys.indexOf('professional') > -1;
            var hasInvestor = keys.indexOf('investor') > -1;

            $scope.hasStartup = hasStartup;
            $scope.hasPro = hasPro;
            $scope.hasInvestor = hasInvestor;

            $rootScope.profiles_count = 0;

            Profile.set_profile_logged(data.result);

            if (!data.err && (hasStartup)) {
                $scope.model.startup = data.result.startup;
                $rootScope.profiles_count++;
            }
            if (!data.err && (hasPro)) {
                $scope.model.professional = data.result.professional;
                $rootScope.profiles_count++;
            }
            if (!data.err && (hasInvestor)) {
                $scope.model.investor = data.result.investor;
                $rootScope.profiles_count++;
            }

            $log.debug($scope.model);
        }

        function onProfileError(data, status) {
            $log.debug('Something goes wrong: ' + data);
        }
        CoreService
            .profile($scope.model.currentUser.id)
            .then(onProfileSuccess, onProfileError);

        function setKeywordHandler(keyword) {
            $scope.keyword = keyword;
            return $scope.keyword;
        }

        function getKeywordHandler() {
            return $scope.keyword;
        }
        $scope.getKeyword = getKeywordHandler;

        function hasKeywordHandler() {
            return !!$scope.keyword;
        }
        $scope.hasKeyword = hasKeywordHandler;

        function isSecondaryHandler(context) {
            return getKeywordHandler() !== context ? 'secondary' : '';
        }
        $scope.isSecondary = isSecondaryHandler;

        function chooseHandler(context) {
            if (context === 'startup') {
                $scope.tab.counter = 0;
            } else if (context === 'pro') {
                $scope.tab.counter = 1;
            } else if (context === 'investor') {
                $scope.tab.counter = 2;
            }
            setKeywordHandler(context);
        }
        $scope.choose = chooseHandler;

        function onSuccess(data) {
            var self = this;
            if (!data.err) {
                $ionicLoading.hide();
                $scope.profiles[self.entity] = data.result;
            }
        }

        function onError(data, status) {
            $ionicLoading.hide();
            $log.debug('Something goes wrong: ' + data);
        }

        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {

                CoreService
                    .list('startup', me.id)
                    .then(onSuccess.bind({
                        entity: 'startup'
                    }), onError);

                CoreService
                    .list('pro', me.id)
                    .then(onSuccess.bind({
                        entity: 'professional'
                    }), onError);

                CoreService
                    .list('investor', me.id)
                    .then(onSuccess.bind({
                        entity: 'investor'
                    }), onError);
            });

        $ionicLoading.show({
            template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
        });

        $timeout(function() {
            $ionicLoading.hide();
            $timeout(function() {
                $scope.hasFinished = true;
            }, 300);
        }, 1000);

        //autocomplete
        function marketsList($query) {
            return CoreService
                .tags('markets', $query);
        }
        $scope.marketsList = marketsList;

        function expertiseList($query) {
            return CoreService
                .tags('expertises', $query);
        }
        $scope.expertiseList = expertiseList;

        function citiesList($query) {
            return CoreService
                .tags('cities', $query);
        }
        $scope.citiesList = citiesList;
    }

    function onNotAuthenticated() {
        $log.debug('dashboard: is not authenticated');
        $state.go('app.intro');
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(me);
            } else {
                onNotAuthenticated();
            }
        });
    }
})
