.controller('ProfileEditInvestorCtrl', function($scope, $state, $log, CoreService, AuthService, $location, $upload, STAGE, $ionicNavBarDelegate) {
  $scope.profile = {};
  $scope.profile.investor = {};

  function onAuthenticated(me) {
      $log.debug('profile-edit-startup: is authenticated');
      $scope.currentUser = me;
      $scope.model.currentUser = me;

      function onReadOneSuccess(data, status) {
          $log.debug('on read one success');
          $('#investimentRangeStealthMode').prop('checked', data.result.investimentRangeStealthMode);
          $('#lookingForIdea').prop('checked', data.result.lookingForIdea);
          $('#lookingForProtoProgress').prop('checked', data.result.lookingForProtoProgress);
          $('#lookingForProtoDone').prop('checked', data.result.lookingForProtoDone);
          $('#lookingForStartupLaunched').prop('checked', data.result.lookingForStartupLaunched);
          $('#lookingForProfitingStartup').prop('checked', data.result.lookingForProfitingStartup);
          $scope.profile.investor = data.result;
      }

      function onReadOneError(data, status) {
          $log.debug('on read one error');
          $log.debug('Something goes wrong: ' + data);
      }

      $scope.goBack = function() {
          $ionicNavBarDelegate.back();
      };

      function editHandler(entity) {
          function onSuccess(data) {
              if (!data.err && data.status) {
                  $scope.goBack();
              }
          }

          function onError(data, status) {
              $log.debug('Something goes wrong: ' + data);
          }

          CoreService
              .edit(entity, {
                  avatar: $scope.profile.investor.avatar,
                  name: $scope.profile.investor.name,
                  country: $scope.profile.investor.country,
                  city: $scope.profile.investor.city,
                  description: $scope.profile.investor.description,
                  expertise: $scope.profile.investor.expertise,
                  favoriteMarket: $scope.profile.investor.favoriteMarket,
                  minInvestimentRange: $scope.profile.investor.minInvestimentRange,
                  maxInvestimentRange: $scope.profile.investor.maxInvestimentRange,
                  investimentRangeStealthMode: $scope.profile.investor.investimentRangeStealthMode,
                  lookingForIdea: $scope.profile.investor.lookingForIdea,
                  lookingForProtoProgress: $scope.profile.investor.lookingForProtoProgress,
                  lookingForProtoDone: $scope.profile.investor.lookingForProtoDone,
                  lookingForStartupLaunched: $scope.profile.investor.lookingForStartupLaunched,
                  lookingForProfitingStartup: $scope.profile.investor.lookingForProfitingStartup
              }, $scope.currentUser.id)
              .then(onSuccess, onError);
      }
      $scope.edit = editHandler;

      function setPercentageHandler(percentage) {
          $scope.percentage = percentage;
      }
      $scope.setPercentage = setPercentageHandler;

      function getPercentageHandler() {
          return $scope.percentage;
      }
      $scope.getPercentage = getPercentageHandler;

      function setInvestorAvatarHandler(avatar) {
          $scope.profile.investor.avatar = avatar;
      }
      $scope.setInvestorAvatar = setInvestorAvatarHandler;

      function onFileProgress(e) {
          var percentage = parseInt(100.0 * e.loaded / e.total);
          $log.debug('precent: ' + percentage + '%');
          setPercentageHandler(percentage);

          document.getElementById('progressBar').style.display = 'block';

          $scope.barProgress = percentage;
      }

      function onFileSuccess(data) {
          document.getElementById('progressBar').style.display = 'none';
          $log.debug('on file success');
          $log.debug(data);
          var avatarURL = 'http://' + STAGE + '/api/avatar/' + data.result._id;

          setInvestorAvatarHandler(avatarURL);
          setPercentageHandler(0);
      }

      function onFileError() {
          $log.debug('on file error');
      }

      function onFileSelect($files, currentType) {
          $log.debug('on file select');
          $log.debug($files);
          for (var i = 0; i < $files.length; i++) {
              var file = $files[i];
              $scope
                  .upload =
                  $upload
                  .upload({
                      url: 'http://' + STAGE + '/api/avatar',
                      file: file,
                      fileFormDataName: 'image'
                  })
                  .progress(onFileProgress)
                  .success(onFileSuccess)
                  .error(onFileError);
          }
      }
      $scope.onFileSelect = onFileSelect;


      $scope.data = {
          "countries": [],
          "cities": []
      };
      //country autocompelte
      function searchCountry() {
          if (
                  $scope.profile.investor.country != undefined 
              &&  $scope.profile.investor.country.length >= 3
          ) {
              var countries = CoreService
                  .tags('countries', $scope.profile.investor.country);
              countries.then(function(c) {
                  $scope.data.countries = c;
              });
          }
      }
      $scope.searchCountry = searchCountry;

      function onClickACCountry(i) {
          $scope.profile.investor.country = $scope.data.countries[i];
          $scope.data.countries = [];
      }
      $scope.onClickACCountry = onClickACCountry;

      //city autocomplete
      var searchCityTimer = null;
      function searchCity() {
          $scope.data.cities = [];
          if (
                  $scope.profile.investor.city != undefined 
              &&  $scope.profile.investor.city.length >= 3
          ) {
              if(searchCityTimer != null) clearTimeout(searchCityTimer);
              searchCityTimer = setTimeout(function() {
                  console.log("here");
                  console.log($scope.profile.investor.city);
                  var cities = CoreService
                      .tags('cities', $scope.profile.investor.city);
                  cities.then(function(c) {
                      $scope.data.cities = c;
                  });
              }, 500);                
          }
      }        
      $scope.searchCity = searchCity;

      function onClickACCity(i) {
          $scope.profile.investor.city = $scope.data.cities[i];
          $scope.data.cities = [];
      }
      $scope.onClickACCity = onClickACCity;

      // onFileUpload($files, currentType, STAGE, $upload, setPercentageHandler, setAvatarHandler, setBarProgress);

      //autocomplete
      function marketsList($query) {
          return CoreService
              .tags('markets', $query);
      }
      $scope.marketsList = marketsList;

      function expertiseList($query) {
          return CoreService
              .tags('expertises', $query);
      }
      $scope.expertiseList = expertiseList;

      AuthService
          .auth(me.identities[0].provider, me)
          .success(function(data, status) {
              CoreService
                  .readOne('investor', me.id, $state.params.profileId)
                  .then(onReadOneSuccess, onReadOneError);
          });
  }

  function onNotAuthenticated() {
      $log.debug('profile-investor-startup: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})
