.controller('MessageCtrl', function($scope, $log, $state, $ionicModal, AuthService, CoreService, $MessageManager, $interval, $ionicScrollDelegate, $ionicNavBarDelegate) {
  $scope.other_guy_id = $state.params.profileId;  

  $scope.getCurrentProfileName = function () {
    return $state.params.name;
  };

  function onSendSuccess(result) {
      $log.debug('send message');
      $log.debug(result);
      $scope.messageContent = '';
  }

  function onSendFail(err) {
      $log.debug(err);
  }

  function sendMessage() {
      if ($scope.messageContent) {
          $MessageManager
              .sendMessage($scope.other_guy_id, $scope.messageContent, $scope.currentUser.id)
              .then(onSendSuccess, onSendFail);
      }
  }
  $scope.sendMessage = sendMessage;

  function conversationManagement() {
      $scope.conversation = [];
      startAt = 0;
      gettingConversation = false;

      var updateConversation = function() {
          if (!gettingConversation) {
              gettingConversation = true;
              $MessageManager.getConversation($state.params.profileId, startAt, $scope.currentUser.id).then(
                  function(conversation) {
                      for (i = 0; i < conversation.length; i++)
                          $scope.conversation.push(conversation[i]);

                      if (conversation.length > 0) {
                          startAt = new Date(conversation[0].updated_at).getTime();
                          $ionicScrollDelegate.scrollBottom();
                      }

                      gettingConversation = false;
                  }
              );
          }
      };

      updateConversation();
      var conversationInterval = $interval(updateConversation, 5000);

      $scope.stopConversaton = function() {
          if (angular.isDefined(conversationInterval)) {
              $interval.cancel(conversationInterval);
              conversationInterval = undefined;
          }
      };

      $scope.$on('$destroy', function() {
          $scope.stopConversaton();
      });
  }

  function onAuthSuccess(data, status) {
      conversationManagement();
  }

  function onAuthenticated(me) {
      $log.debug('messages: is authenticated');
      $scope.model.currentUser = me;
      $scope.currentUser = me;

      AuthService
          .auth(me.identities[0].provider, me)
          .success(onAuthSuccess);
  }

  function onNotAuthenticated() {
      $log.debug('messages: is not authenticated');
      $state.go('app.intro');
  }

  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})
