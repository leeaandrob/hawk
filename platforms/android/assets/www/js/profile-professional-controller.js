.controller('ProfileProfessionalCtrl', function($scope, $state, $log, $ionicModal, $ionicPopup, CoreService, AuthService, $location, STAGE, $upload, Profile) {
  $scope.profile = {};

  function onAuthenticated(me) {
      $log.debug('profile-single: is authenticated');
      $scope.currentUser = me;
      $scope.model.currentUser = me;
      $scope.editMode = ($location.search().edit === 'true') || Profile.is_my_profile($state.params.profileId);

      function sendMessageHandler(profile) {
          $log.debug('Send message to ' + profile.title);
          $ionicModal
              .fromTemplateUrl('templates/send-message.html', {
                  scope: $scope
              })
              .then(function(modal) {
                  $scope.modal = modal;
                  $scope.modal.show();

                  $scope.send = function() {
                      $scope.modal.hide();
                  };
              });
      }
      $scope.sendMessage = sendMessageHandler;

      function recommendHandler(profile) {
          $log.debug('Recommend ' + profile.title);

          function onRecommendSuccess(data) {
              $log.debug('on success handler');
              if (!data.err) {
                  $log.debug(profile.name + ' was recommended!');
                  $scope.hasRecommend = true;
                  $scope.recommendationCounter += 1;
              }
          }

          function onRecommendError(data, status) {
              $log.debug('on error handler');
              $log.debug('Something goes wrong: ' + data);
          }

          $log.debug('Profile ' + profile.name + ' recommended!');
          CoreService
              .recommend(me.id, {
                  recommend: profile.profile_id._id || profile.profile_id,
                  type: 'pro'
              })
              .then(onRecommendSuccess, onRecommendError);
      }
      $scope.recommend = recommendHandler;

      function unrecommendHandler(profile) {
          $log.debug('unrecommend ' + profile.name);

          function onUnrecommendSuccess(data) {
              $log.debug('on success handler');
              if (!data.err) {
                  $log.debug(profile.name + ' was unrecommend!');
                  $scope.hasRecommend = false;
                  $scope.recommendationCounter -= 1;
              }
          }

          function onUnrecommendError(data, status) {
              $log.debug('on error handler');
              $log.debug('Something goes wrong: ' + data);
          }

          CoreService
              .unrecommend(me.id, {
                  recommend: profile.profile_id._id || profile.profile_id,
                  type: 'pro'
              })
              .then(onUnrecommendSuccess, onUnrecommendError);
      }
      $scope.unrecommend = unrecommendHandler;

      function favoriteHandler(profile) {
          $log.debug('favorite ' + profile.name);

          function onFavoriteSuccess(data) {
              $log.debug('on success handler');
              if (!data.err) {
                  $log.debug(profile.name + ' was favorited!');
                  $scope.hasFavorite = true;
              }
          }

          function onFavoriteError(data, status) {
              $log.debug('on error handler');
              $log.debug('Something goes wrong: ' + data);
          }

          CoreService
              .favorite(me.id, {
                  favorite: profile.profile_id._id || profile.profile_id,
                  type: 'pro'
              })
              .then(onFavoriteSuccess, onFavoriteError);
      }
      $scope.favorite = favoriteHandler;

      function unfavoriteHandler(profile) {
          $log.debug('unfavorite ' + profile.name);

          function onUnfavoriteSuccess(data) {
              $log.debug('on success handler');
              if (!data.err) {
                  $log.debug(profile.name + ' was unfavorited!');
                  $scope.hasFavorite = false;
              }
          }

          function onUnfavoriteError(data, status) {
              $log.debug('on error handler');
              $log.debug('Something goes wrong: ' + data);
          }

          CoreService
              .unfavorite(me.id, {
                  favorite: profile.profile_id._id || profile.profile_id,
                  type: 'pro'
              })
              .then(onUnfavoriteSuccess, onUnfavoriteError);
      }
      $scope.unfavorite = unfavoriteHandler;

      function onHasFavoriteSuccess(data) {
          $log.debug('on success handler');
          if (!data.err) {
              $scope.hasFavorite = data.result;
          }
      }

      function onHasFavoriteError(data, status) {
          $log.debug('on error handler');
          $log.debug('Something goes wrong: ' + data);
      }

      function onHasRecommendSuccess(data) {
          $log.debug('on success handler');
          if (!data.err) {
              $scope.hasRecommend = data.result;
          }
      }

      function onHasRecommendError(data, status) {
          $log.debug('on error handler');
          $log.debug('Something goes wrong: ' + data);
      }

      function onRecommendCounterSuccess(data) {
          $log.debug('on success handler');
          if (!data.err) {
              $scope.recommendationCounter = data.result;
          }
      }

      function onRecommendCounterError(data, status) {
          $log.debug('on error handler');
          $log.debug('Something goes wrong: ' + data);
      }

      function onReadOneSuccess(data) {
          $log.debug('on success handler');
          if (!data.err) {
              console.log('DATARESULT');
              $scope.profile = data.result;
              $scope.profile.type = 'professional';
              $log.debug($scope.profile.type);
              console.log($scope.profile);
              CoreService
                  .hasFavorite(me.id, $scope.profile.profile_id._id || $scope.profile.profile_id)
                  .then(onHasFavoriteSuccess, onHasFavoriteError);

              CoreService
                  .hasRecommend(me.id, $scope.profile.profile_id._id || $scope.profile.profile_id)
                  .then(onHasRecommendSuccess, onHasRecommendError);

              CoreService
                  .recommendCounter(me.id, $scope.profile.profile_id._id || $scope.profile.profile_id)
                  .then(onRecommendCounterSuccess, onRecommendCounterError);
          }
      }

      $scope.testeid = $state.params.profileId;

      function onReadOneError(data, status) {
          $log.debug('on error handler');
          $log.debug('Something goes wrong: ' + data);
      }

      AuthService
          .auth(me.identities[0].provider, me)
          .success(function(data, status) {
              CoreService
                  .readOne('professional', me.id, $state.params.profileId)
                  .then(onReadOneSuccess, onReadOneError);
          });
  }

  function onNotAuthenticated() {
      $log.debug('profile-single: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})
