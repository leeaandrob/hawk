angular
  .module('starter.controllers', [])

.controller('AboutCtrl', function($scope, $state, $log, AuthService) {
  function onAuthenticated(me) {
      $log.debug('about: is authenticated');
  }

  function onNotAuthenticated() {
      $log.debug('settings: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})

.controller('AppCtrl', function($scope, $log, $state, AuthService, $ionicLoading, $timeout, $window) {
    $scope.hasFinished = false;
    $scope.fixSair = $(window).height() - 243 + 'px';
    $scope.model = {
        currentUser: {}
    };

    $scope.login = AuthService.login;
    $scope.browserEnabled = /Mobile|Chrome/.test($window.navigator.userAgent);

    function onAuthenticated(me) {
        $scope.me = me;
        $log.debug('app: authenticated');
        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {
                $state.go('app.register');
            });
    }

    function onNotAuthenticated() {
        $log.debug('app: is not authenticated');
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(window.Hull.currentUser());
            } else {
                onNotAuthenticated();
            }
        });

        window.Hull.on('hull.auth.login', function(me) {
            if (me) {
                onAuthenticated(me);
            } else {
                onNotAuthenticated();
            }
        });
    }

    $ionicLoading.show({
        template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
    });

    $timeout(function() {
        $ionicLoading.hide();
        $timeout(function() {
            $scope.hasFinished = true;
        }, 300);
    }, 5000);
})

.controller('AutocompleteCtrl', ['$scope', 'AutoCompleteService', function($scope, AutoCompleteService) {
  $scope.myTitle = 'Auto Complete';

  $scope.data = {
      "airlines": [],
      "search": ''
  };

  $scope.search = function() {
      AutoCompleteService.searchAirlines($scope.data.search).then(
          function(matches) {
              $scope.data.airlines = matches;
          }
      )
  };
}])

.controller('BlogCtrl', function($scope, $log, $state, CoreService, AuthService) {
    function onAuthenticated(me) {
        $log.debug('blog: is authenticated');
        $scope.model.currentUser = me;
        $scope.currentUser = me;

        function onSuccess(data) {
            $log.debug('on success handler');
            $scope.posts = data.result;
        }

        function onError(data, status) {
            $log.debug('on error handler');
            $log.debug('Something goes wrong: ' + data);
        }
        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {
                CoreService
                    .blogPosts(me.id)
                    .then(onSuccess, onError);
            });
    }

    function onNotAuthenticated() {
        $log.debug('favorites: is not authenticated');
        $state.go('app.intro');
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(window.Hull.currentUser());
            } else {
                onNotAuthenticated();
            }
        });
    }
})

.controller('EventsCtrl', function($scope, $log, $state, CoreService, AuthService) {
    function onAuthenticated(me) {
        $log.debug('blog: is authenticated');
        $scope.model.currentUser = me;
        $scope.currentUser = me;

        function onSuccess(data) {
            $log.debug('on success handler');

            function filterPosts(post) {
                if (post.tags.indexOf('evento') > -1) {
                    return post;
                }
            }
            $scope.posts = data.result.filter(filterPosts);
        }

        function onError(data, status) {
            $log.debug('on error handler');
            $log.debug('Something goes wrong: ' + data);
        }
        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {
                CoreService
                    .blogPosts(me.id)
                    .then(onSuccess, onError);
            });
    }

    function onNotAuthenticated() {
        $log.debug('favorites: is not authenticated');
        $state.go('app.intro');
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(window.Hull.currentUser());
            } else {
                onNotAuthenticated();
            }
        });
    }
})

.controller('PostCtrl', function($scope, $log, $state, CoreService, AuthService) {
    function onAuthenticated(me) {
        $log.debug('post: is authenticated');
        $scope.model.currentUser = me;
        $scope.currentUser = me;

        function onSuccess(data) {
            $log.debug('on success handler');
            $log.debug('post id: ' + $state.params.id);

            function filterPost(post) {
                $log.debug('post id in list: ' + post.id);
                if (post.id.toString() === $state.params.id.toString()) {
                    $log.debug('post matched!');
                    return post;
                }
            }
            $scope.post = data.result.filter(filterPost)[0];
        }

        function onError(data, status) {
            $log.debug('on error handler');
            $log.debug('Something goes wrong: ' + data);
        }
        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {
                CoreService
                    .blogPosts(me.id)
                    .then(onSuccess, onError);
            });
    }

    function onNotAuthenticated() {
        $log.debug('favorites: is not authenticated');
        $state.go('app.intro');
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(window.Hull.currentUser());
            } else {
                onNotAuthenticated();
            }
        });
    }
})

.controller('ContactCtrl', function($scope, $state, $log, AuthService, CoreService) {
  $scope.feedback = {};

  function onAuthenticated(me) {
      $log.debug('about: is authenticated');
      $('.message-thanks').fadeOut(1);

      function onSendFeedbackSuccess(data) {
          $log.debug('on send feedback success');
          $scope.feedback.body = '';
      }

      function onSendFeedbackError(data, status) {
          $log.debug('on send feedback error');
      }

      function send(feedback) {
          CoreService
              .sendFeedback(me.id, feedback)
              .then(onSendFeedbackSuccess, onSendFeedbackError);

          $('.message-thanks').fadeIn(300);

      }
      $scope.send = send;
  }

  function onNotAuthenticated() {
      $log.debug('settings: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})

.controller('DashboardCtrl', function($scope, $rootScope, $log, $state, AuthService, CoreService, $ionicLoading, $timeout, $ionicScrollDelegate, $ionicModal, Profile, $localStorage, $MessageManager, StartupManager) {
    

    $scope.hasFinished = false;
    $scope.tab = {
        counter: 0,
        selected: ''
    };
    $scope.profiles = {
        startup: [],
        professional: [],
        investor: []
    };
    $scope.modal = {
        hide: function() {}
    };
    $scope.searched = false;
    $scope.lastSearch = '';
    $scope.search = {
      startup: $localStorage.startup || {},
      pro: $localStorage.pro || {},
      investor: $localStorage.investor || {}
    };

    $scope.handleEnterKey = function(keyEvent) {
        if (keyEvent.keyCode === 13){
            keyEvent.preventDefault();
            return false;
        }
    };

    CoreService.profile($scope.currentUser.id).then(
        function(data) {
            var keys = (!data.err && (data.result)) ? Object.keys(data.result) : [];
            var hasPro = keys.indexOf('professional') > -1;
            var hasInvestor = keys.indexOf('investor') > -1;

            if (!data.err && (hasPro)) {
                $scope.model.professional = data.result.professional;
                console.log($scope.model.professional)

                StartupManager.getInvites($scope.model.professional, 'pro', $scope.currentUser.id).then(
                    function(startup_invites) {
                        $scope.startup_invites_pro = startup_invites;
                    }
                );
            } else if (!data.err && (hasInvestor)) {
                $scope.model.investor = data.result.investor;
                console.log($scope.model.investor);

                StartupManager.getInvites($scope.model.investor, 'investor', $scope.currentUser.id).then(
                    function(startup_invites) {
                        $scope.startup_invites_investor = startup_invites;
                    }
                );
            }

        }
    );

    $scope.confirmMember = function(type, startup_invite, startup_invites_index) {
        if (type === 'pro') {
          StartupManager.confirmMember($scope.model.professional, type, startup_invite, $scope.currentUser.id).then(
            function(result) {
                $scope.startup_invites_pro.pop(startup_invites_index);
                console.log(result);
            },
            function(error) {
                console.log(error);
            }
          );
        } else if (type === 'investor') {
         StartupManager.confirmMember($scope.model.investor, type, startup_invite, $scope.currentUser.id).then(
            function(result) {
                $scope.startup_invites_investor.pop(startup_invites_index);
                console.log(result);
            },
            function(error) {
                console.log(error);
            }
          ); 
        }
    }

    $scope.refuseMember = function(type, startup_invite, startup_invites_index) {
        StartupManager.refuseMember($scope.model.professional, type, startup_invite, $scope.currentUser.id).then(
            function(result) {
                if (type == 'pro') {
                    $scope.startup_invites_pro.pop(startup_invites_index);
                } else {
                    $scope.startup_invites_investor.pop(startup_invites_index);
                }
                console.log(result);
            },
            function(error) {
                console.log(error);
            }
        );
    }

    function onAuthenticated(me) {
        $log.debug('dashboard: is authenticated');
        $scope.keyword = setCurrentHandler();

        $MessageManager.getInboxMessages($scope.currentUser.id || me.id).then(
            function(messages) {
                $scope.messagesCounter = messages.length;
            }
        );

        function onSwipeLeft() {
            $log.debug('swipe left');
            $scope.tab.counter += 1;
            if ($scope.tab.counter === 1) {
                $scope.choose('pro');
            } else if ($scope.tab.counter === 2) {
                $scope.choose('investor');
            } else if ($scope.tab.counter === 3) {
                $scope.choose('startup');
            } else {
                $scope.choose('startup');
                $scope.tab.counter = 0;
            }
        }
        $scope.onSwipeLeft = onSwipeLeft;

        function onSearchSuccess(data) {
            $log.debug('on success handler');
            var self = this;
            $log.debug('entity: ' + self.entity);
            $ionicScrollDelegate.scrollTop();
            if (!data.err) {
                $scope.choose(self.entity === 'professional' ? 'pro' : self.entity);
                $scope.profiles[self.entity] = data.result;
                $scope.searched = true;
                $scope.lastSearch = self.entity;
                $scope.modal.hide();
            }
            $ionicLoading.hide();
        }

        function cleanSearch() {
            $scope.searched = false;

            $ionicLoading.show({
                template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
            });

            if ($scope.lastSearch === 'startup') {
                CoreService
                    .list('startup', me.id)
                    .then(onSuccess.bind({
                        entity: 'startup'
                    }), onError);
            } else if ($scope.lastSearch === 'professional') {
                CoreService
                    .list('pro', me.id)
                    .then(onSuccess.bind({
                        entity: 'professional'
                    }), onError);
            } else if ($scope.lastSearch === 'investor') {
                CoreService
                    .list('investor', me.id)
                    .then(onSuccess.bind({
                        entity: 'investor'
                    }), onError);
            }
        }
        $scope.cleanSearch = cleanSearch;

        function onSearchError(data, status) {
            $log.debug('on error handler');
            $log.debug('Something goes wrong: ' + data);
            $ionicLoading.hide();
        }

        function searchStartup() {
            $log.debug('search startup handler');
            $log.debug($scope.search);
            $localStorage.startup = $scope.search.startup;
            $log.debug($localStorage.startup);
            $ionicLoading.show({
                template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
            });
            CoreService
                .search('startup', me.id, $scope.search.startup)
                .then(onSearchSuccess.bind({
                    entity: 'startup'
                }), onSearchError);
        }
        $scope.searchStartup = searchStartup;

        function searchPro() {
            $log.debug('search professional handler');
            $localStorage.pro = $scope.search.pro;
            $log.debug($localStorage.pro);
            $ionicLoading.show({
                template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
            });
            CoreService
                .search('pro', me.id, $scope.search.pro)
                .then(onSearchSuccess.bind({
                    entity: 'professional'
                }), onSearchError);
        }
        $scope.searchPro = searchPro;

        function searchInvestor() {
            $log.debug('search investor handler');
            $localStorage.investor = $scope.search.investor;
            $ionicLoading.show({
                template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
            });
            CoreService
                .search('investor', me.id, $scope.search.investor)
                .then(onSearchSuccess.bind({
                    entity: 'investor'
                }), onSearchError);
        }
        $scope.searchInvestor = searchInvestor;

        function enableState() {
            $ionicModal.fromTemplateUrl('templates/explore.html', {
                scope: $scope,
                animation: 'slide-in-left'
            }).then(function(modal) {
                $scope.modal = modal;
                modal.show();
            });
        }
        $scope.enableState = enableState;

        function setCurrentHandler() {
            var out = 'startup';
            var hasStartup = $scope.profiles.startup.length > 0;
            var hasPro = $scope.profiles.professional.length > 0;
            var hasInvestor = $scope.profiles.investor.length > 0;

            if (hasStartup) {
                out = 'startup';
            } else if (hasPro) {
                out = 'pro';
            } else if (hasInvestor) {
                out = 'investor';
            }

            return out;
        }

        function isCurrentHandler(current) {
            var out = false;

            if (hasKeywordHandler()) {
                out = getKeywordHandler() === current;
            }

            return out;
        }
        $scope.isCurrent = isCurrentHandler;

        function onProfileSuccess(data) {
            $log.debug(data);
            var keys = (!data.err && (data.result)) ? Object.keys(data.result) : [];
            var hasStartup = keys.indexOf('startup') > -1;
            var hasPro = keys.indexOf('professional') > -1;
            var hasInvestor = keys.indexOf('investor') > -1;

            $scope.hasStartup = hasStartup;
            $scope.hasPro = hasPro;
            $scope.hasInvestor = hasInvestor;

            $rootScope.profiles_count = 0;

            Profile.set_profile_logged(data.result);

            if (!data.err && (hasStartup)) {
                $scope.model.startup = data.result.startup;
                $rootScope.profiles_count++;
            }
            if (!data.err && (hasPro)) {
                $scope.model.professional = data.result.professional;
                $rootScope.profiles_count++;
            }
            if (!data.err && (hasInvestor)) {
                $scope.model.investor = data.result.investor;
                $rootScope.profiles_count++;
            }

            $log.debug($scope.model);
        }

        function onProfileError(data, status) {
            $log.debug('Something goes wrong: ' + data);
        }
        CoreService
            .profile($scope.model.currentUser.id)
            .then(onProfileSuccess, onProfileError);

        function setKeywordHandler(keyword) {
            $scope.keyword = keyword;
            return $scope.keyword;
        }

        function getKeywordHandler() {
            return $scope.keyword;
        }
        $scope.getKeyword = getKeywordHandler;

        function hasKeywordHandler() {
            return !!$scope.keyword;
        }
        $scope.hasKeyword = hasKeywordHandler;

        function isSecondaryHandler(context) {
            return getKeywordHandler() !== context ? 'secondary' : '';
        }
        $scope.isSecondary = isSecondaryHandler;

        function chooseHandler(context) {
            if (context === 'startup') {
                $scope.tab.counter = 0;
            } else if (context === 'pro') {
                $scope.tab.counter = 1;
            } else if (context === 'investor') {
                $scope.tab.counter = 2;
            }
            setKeywordHandler(context);
        }
        $scope.choose = chooseHandler;

        function onSuccess(data) {
            var self = this;
            if (!data.err) {
                $ionicLoading.hide();
                $scope.profiles[self.entity] = data.result;
            }
        }

        function onError(data, status) {
            $ionicLoading.hide();
            $log.debug('Something goes wrong: ' + data);
        }

        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {

                CoreService
                    .list('startup', me.id)
                    .then(onSuccess.bind({
                        entity: 'startup'
                    }), onError);

                CoreService
                    .list('pro', me.id)
                    .then(onSuccess.bind({
                        entity: 'professional'
                    }), onError);

                CoreService
                    .list('investor', me.id)
                    .then(onSuccess.bind({
                        entity: 'investor'
                    }), onError);
            });

        $ionicLoading.show({
            template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
        });

        $timeout(function() {
            $ionicLoading.hide();
            $timeout(function() {
                $scope.hasFinished = true;
            }, 300);
        }, 1000);

        //autocomplete
        function marketsList($query) {
            return CoreService
                .tags('markets', $query);
        }
        $scope.marketsList = marketsList;

        function expertiseList($query) {
            return CoreService
                .tags('expertises', $query);
        }
        $scope.expertiseList = expertiseList;

        function citiesList($query) {
            return CoreService
                .tags('cities', $query);
        }
        $scope.citiesList = citiesList;
    }

    function onNotAuthenticated() {
        $log.debug('dashboard: is not authenticated');
        $state.go('app.intro');
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(me);
            } else {
                onNotAuthenticated();
            }
        });
    }
})

.controller('ExploreCtrl', function($scope, $state, $log, CoreService, AuthService, STAGE, $http, $location, $ionicLoading, $ionicScrollDelegate, $window) {
    $log.debug('explore controller');
    //$scope.search = {};
    //$scope.search.pro = {};
    //$scope.search.startup = ($scope.search.startup || {});
    //$scope.search.investor = {};

    $log.debug($window.localStorage.getItem('location'));

    function onAuthenticated(me) {
        $log.debug('explore: is authenticated');
        $scope.currentUser = me;
        $scope.model.currentUser = me;
        $scope.keyword = '';

        function getCurrentHandler() {
            return $location.search().current;
        }
        $scope.getCurrent = getCurrentHandler;

        function getKeywordHandler() {
            return $scope.keyword;
        }
        $scope.getKeyword = getKeywordHandler;

        function isSecondaryHandler(context) {
            return getKeywordHandler() !== context ? 'secondary' : '';
        }
        $scope.isSecondary = isSecondaryHandler;

        function chooseHandler(context) {
            $scope.keyword = context;
        }
        $scope.choose = chooseHandler;

        // set default context
        chooseHandler(getCurrentHandler() || 'startup');

        //autocomplete
        function marketsList($query) {
            return CoreService
                .tags('markets', $query);
        }
        $scope.marketsList = marketsList;

        function expertiseList($query) {
            return CoreService
                .tags('expertises', $query);
        }
        $scope.expertiseList = expertiseList;

        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {
                function onSuccess(data) {
                    $log.debug('on success handler');
                    var self = this;
                    $log.debug('entity: ' + self.entity);
                    $ionicScrollDelegate.scrollTop();
                    if (!data.err) {
                        $scope.profiles.startup = [];
                        $scope.profiles.professional = [];
                        $scope.profiles.investor = [];
                        $scope.profiles[self.entity] = data.result;
                    }
                    $ionicLoading.hide();
                }

                function onError(data, status) {
                    $log.debug('on error handler');
                    $log.debug('Something goes wrong: ' + data);
                    $ionicLoading.hide();
                }

                function searchStartup(search, keyword) {
                    $log.debug('search startup handler');
                    $log.debug(search);
                    chooseHandler(keyword);
                    $ionicLoading.show({
                        template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
                    });
                    $window.localStorage.setItem('location', search.startup.location);
                    $log.debug($window.localStorage.getItem('location'));
                    CoreService
                        .search('startup', me.id, search.startup)
                        .then(onSuccess.bind({
                            entity: 'startup'
                        }), onError);
                }
                $scope.searchStartup = searchStartup;

                function searchPro(search) {
                    $log.debug('search professional handler');
                    $log.debug(search);
                    $ionicLoading.show({
                        template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
                    });
                    localStorage.location = search.pro.location;
                    $log.debug(localStorage.location);
                    CoreService
                        .search('pro', me.id, search.pro)
                        .then(onSuccess.bind({
                            entity: 'professional'
                        }), onError);
                }
                $scope.searchPro = searchPro;

                function searchInvestor(search) {
                    $log.debug('search investor handler');
                    $log.debug(search);
                    $ionicLoading.show({
                        template: '<i class="ion-loading-a" data-animation="true" data-pack="default"></i>'
                    });
                    localStorage.location = search.investor.location;
                    $log.debug(localStorage.location);
                    CoreService
                        .search('investor', me.id, search.investor)
                        .then(onSuccess.bind({
                            entity: 'investor'
                        }), onError);
                }
                $scope.searchInvestor = searchInvestor;
            });
    }

    function onNotAuthenticated() {
        $log.debug('explore: is not authenticated');
        $state.go('app.intro');
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(window.Hull.currentUser());
            } else {
                onNotAuthenticated();
            }
        });
    }
})

.controller('FavoritesCtrl', function($scope, $log, $state, CoreService, AuthService) {
  $scope.favorites = {
      professional: [],
      startup: [],
      investor: [],
  };

  function onAuthenticated(me) {
      $log.debug('favorites: is authenticated');
      $scope.model.currentUser = me;
      $scope.currentUser = me;

      function profileMapHandler(item) {
        $log.debug(item);
        var type = (item.type === 'pro' ? 'professional' : item.type);
        $scope.favorites[type].push(item);
      }

      function onSuccess(data) {
          $log.debug('on success handler');
          $log.debug('data');
          $log.debug(data.result);
          data.result.map(profileMapHandler);
      }

      function onError(data, status) {
          $log.debug('on error handler');
          $log.debug('Something goes wrong: ' + data);
      }
      AuthService
          .auth(me.identities[0].provider, me)
          .success(function(data, status) {
              CoreService
                  .favoriteList(me.id)
                  .then(onSuccess, onError);
          });
  }

  function onNotAuthenticated() {
      $log.debug('favorites: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})

.controller('IntroCtrl', function($scope, $log, $state, AuthService) {
    $log.debug('intro controller');
    $scope.intro = true;

    function onAuthenticated(me) {
        $log.debug('intro: authenticated');
        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {
                $state.go('app.register');
            });
    }

    function onNotAuthenticated() {
        $log.debug('intro: not authenticated');
        $scope.login = AuthService.login;
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(me);
            } else {
                onNotAuthenticated();
            }
        });

        window.Hull.on('hull.auth.login', function(me) {
            if (me) {
                onAuthenticated(me);
            } else {
                onNotAuthenticated();
            }
        });
    }
})

.controller('LogoutCtrl', function($state, $log, AuthService) {
    window.Hull.logout(function() {
        $state.go('app.intro');
    });
})

.controller('MessageCtrl', function($scope, $log, $state, $ionicModal, AuthService, CoreService, $MessageManager, $interval, $ionicScrollDelegate, $ionicNavBarDelegate) {
  $scope.other_guy_id = $state.params.profileId;  

  $scope.getCurrentProfileName = function () {
    return $state.params.name;
  };

  function onSendSuccess(result) {
      $log.debug('send message');
      $log.debug(result);
      $scope.messageContent = '';
  }

  function onSendFail(err) {
      $log.debug(err);
  }

  function sendMessage() {
      if ($scope.messageContent) {
          $MessageManager
              .sendMessage($scope.other_guy_id, $scope.messageContent, $scope.currentUser.id)
              .then(onSendSuccess, onSendFail);
      }
  }
  $scope.sendMessage = sendMessage;

  function conversationManagement() {
      $scope.conversation = [];
      startAt = 0;
      gettingConversation = false;

      var updateConversation = function() {
          if (!gettingConversation) {
              gettingConversation = true;
              $MessageManager.getConversation($state.params.profileId, startAt, $scope.currentUser.id).then(
                  function(conversation) {
                      for (i = 0; i < conversation.length; i++)
                          $scope.conversation.push(conversation[i]);

                      if (conversation.length > 0) {
                          startAt = new Date(conversation[0].updated_at).getTime();
                          $ionicScrollDelegate.scrollBottom();
                      }

                      gettingConversation = false;
                  }
              );
          }
      };

      updateConversation();
      var conversationInterval = $interval(updateConversation, 5000);

      $scope.stopConversaton = function() {
          if (angular.isDefined(conversationInterval)) {
              $interval.cancel(conversationInterval);
              conversationInterval = undefined;
          }
      };

      $scope.$on('$destroy', function() {
          $scope.stopConversaton();
      });
  }

  function onAuthSuccess(data, status) {
      conversationManagement();
  }

  function onAuthenticated(me) {
      $log.debug('messages: is authenticated');
      $scope.model.currentUser = me;
      $scope.currentUser = me;

      AuthService
          .auth(me.identities[0].provider, me)
          .success(onAuthSuccess);
  }

  function onNotAuthenticated() {
      $log.debug('messages: is not authenticated');
      $state.go('app.intro');
  }

  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})

.controller('MessagesCtrl', function($scope, $log, $state, AuthService, CoreService, $MessageManager, StartupManager) {
  function onAuthSuccess(data, status) {
      $MessageManager.getInboxMessages($scope.currentUser.id).then(
          function(messages) {
              $scope.messages = messages;
          }
      );

      $scope.openStartupProfile = function(startup) {
          console.log(startup);
          $state.go('app.startup', {
              profileId: startup._id
          });
      }
  }

  function onAuthenticated(me) {
      $log.debug('messages: is authenticated');
      $scope.model.currentUser = me;
      $scope.currentUser = me;

      AuthService
          .auth(me.identities[0].provider, me)
          .success(onAuthSuccess);
  }

  function onNotAuthenticated() {
      $log.debug('messages: is not authenticated');
      $state.go('app.intro');
  }

  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})

.controller('ProfileCtrl', function($scope, $state, $log, $ionicModal, AuthService, CoreService, $rootScope) {
  $scope.profile = {
      startup: {},
      professional: {},
      investor: {},
  };

  function onAuthenticated(me) {
      $log.debug('on authenticated');
      $scope.model.currentUser = me;
      $scope.currentUser = me;

      function onSuccess(data) {
          var keys = (!data.err && (data.result)) ? Object.keys(data.result) : [];
          var hasStartup = keys.indexOf('startup') > -1;
          var hasPro = keys.indexOf('professional') > -1;
          var hasInvestor = keys.indexOf('investor') > -1;

          $scope.hasStartup = hasStartup;
          $scope.hasPro = hasPro;
          $scope.hasInvestor = hasInvestor;

          $rootScope.$broadcast('profile', data.result);

          if (!data.err && (hasStartup)) {
              $scope.profile.startup = data.result.startup;
          }
          if (!data.err && (hasPro)) {
              $scope.profile.professional = data.result.professional;
          }
          if (!data.err && (hasInvestor)) {
              $scope.profile.investor = data.result.investor;
          }
      }

      function onError(data, status) {
          $log.debug('Something goes wrong: ' + data);
      }
      AuthService
          .auth(me.identities[0].provider, me)
          .success(function(data, status) {
              CoreService
                  .profile($scope.currentUser.id)
                  .then(onSuccess, onError);
          });
  }

  function onNotAuthenticated() {
      $log.debug('profile: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})

.controller('ProfileEditStartupCtrl', function($scope, $state, $log, CoreService, AuthService, $location, $upload, STAGE, $ionicNavBarDelegate) {
  $scope.profile = {};
  $scope.profile.startup = {};
  $scope.profile.tmp = {};
  $scope.profile.team = [];

  function onAuthenticated(me) {
      $log.debug('profile-edit-startup: is authenticated');
      $scope.currentUser = me;
      $scope.model.currentUser = me;

      function isMemberValid(member) {
          return member.lookingForExpertise.length > 0 && ( member.offerEquity || member.offerCash );
      }

      function addToTeam(member) {
          if( isMemberValid(member) ) {
              $scope.profile.startup.team.push(member);
              $scope.profile.tmp = {};
              $('#offerEquity').prop('checked', false);
              $('#offerCash').prop('checked', false);
          }
      }
      $scope.addToTeam = addToTeam;

      function removeTagFromTeam(parentIndex, i) {
          $scope.profile.startup.team[parentIndex].lookingForExpertise.splice(i, 1);
          if (!$scope.profile.startup.team[parentIndex].lookingForExpertise.length) {
              removeFromTeam(parentIndex);
          }
      }
      $scope.removeTagFromTeam = removeTagFromTeam;

      function removeFromTeam(i) {
          $scope.profile.startup.team.splice(i, 1);
      }

      function onReadOneSuccess(data, status) {
          $log.debug('on read one success');
          $('#lookingForPros').prop('checked', data.result.lookingForPros);
          $('#lookingForInvestor').prop('checked', data.result.lookingForInvestor);
          $scope.profile.team = data.result.team;
          console.log(data.result);
          $scope.profile.startup = data.result;
      }

      function onReadOneError(data, status) {
          $log.debug('on read one error');
          $log.debug('Something goes wrong: ' + data);
      }

      $scope.goBack = function() {
          $ionicNavBarDelegate.back();
      };

      function editHandler(entity) {
          function onSuccess(data) {
              if (!data.err && data.status) {
                  $scope.goBack();
              }
          }

          function onError(data, status) {
              $log.debug('Something goes wrong: ' + data);
          }

          CoreService
              .edit(entity, {
                  avatar: $scope.profile.startup.avatar,
                  name: $scope.profile.startup.name,
                  site: $scope.profile.startup.site,
                  country: $scope.profile.startup.country,
                  city: $scope.profile.startup.city,
                  tagline: $scope.profile.startup.tagline,
                  description: $scope.profile.startup.description,
                  market: $scope.profile.startup.market,
                  developmentStage: $scope.profile.startup.developmentStage,
                  lookingForPros: $scope.profile.startup.lookingForPros,
                  team: $scope.profile.startup.team,
                  lookingForInvestor: $scope.profile.startup.lookingForInvestor
              }, $scope.currentUser.id)
              .then(onSuccess, onError);
      }
      $scope.edit = editHandler;

      function setPercentageHandler(percentage) {
          $scope.percentage = percentage;
      }
      $scope.setPercentage = setPercentageHandler;

      function getPercentageHandler() {
          return $scope.percentage;
      }
      $scope.getPercentage = getPercentageHandler;

      function setStartupAvatarHandler(avatar) {
          $scope.profile.startup.avatar = avatar;
      }
      $scope.setStartupAvatar = setStartupAvatarHandler;

      function onFileProgress(e) {
          var percentage = parseInt(100.0 * e.loaded / e.total);
          $log.debug('precent: ' + percentage + '%');
          setPercentageHandler(percentage);

          document.getElementById('progressBar').style.display = 'block';

          $scope.barProgress = percentage;
      }

      function onFileSuccess(data) {
          document.getElementById('progressBar').style.display = 'none';
          $log.debug('on file success');
          $log.debug(data);
          var avatarURL = 'http://' + STAGE + '/api/avatar/' + data.result._id;

          setStartupAvatarHandler(avatarURL);
          setPercentageHandler(0);
      }

      function onFileError() {
          $log.debug('on file error');
      }

      function onFileSelect($files, currentType) {
          $log.debug('on file select');
          $log.debug($files);
          for (var i = 0; i < $files.length; i++) {
              var file = $files[i];
              $scope
                  .upload =
                  $upload
                  .upload({
                      url: 'http://' + STAGE + '/api/avatar',
                      file: file,
                      fileFormDataName: 'image'
                  })
                  .progress(onFileProgress)
                  .success(onFileSuccess)
                  .error(onFileError);
          }
      }
      $scope.onFileSelect = onFileSelect;

      $scope.data = {
          "countries": [],
          "cities": []
      };
      //country autocompelte
      function searchCountry() {
          if (
                  $scope.profile.startup.country != undefined 
              &&  $scope.profile.startup.country.length >= 3
          ) {
              var countries = CoreService
                  .tags('countries', $scope.profile.startup.country);
              countries.then(function(c) {
                  $scope.data.countries = c;
              });
          }
      }
      $scope.searchCountry = searchCountry;

      function onClickACCountry(i) {
          $scope.profile.startup.country = $scope.data.countries[i];
          $scope.data.countries = [];
      }
      $scope.onClickACCountry = onClickACCountry;

      //city autocomplete
      var searchCityTimer = null;
      function searchCity() {
          $scope.data.cities = [];
          if (
                  $scope.profile.startup.city != undefined 
              &&  $scope.profile.startup.city.length >= 3
          ) {
              if(searchCityTimer != null) clearTimeout(searchCityTimer);
              searchCityTimer = setTimeout(function() {
                  console.log("here");
                  console.log($scope.profile.startup.city);
                  var cities = CoreService
                      .tags('cities', $scope.profile.startup.city);
                  cities.then(function(c) {
                      $scope.data.cities = c;
                  });
              }, 500);                
          }
      }
      $scope.searchCity = searchCity;

      function onClickACCity(i) {
          $scope.profile.startup.city = $scope.data.cities[i];
          $scope.data.cities = [];
      }
      $scope.onClickACCity = onClickACCity;

      //autocomplete
      function marketsList($query) {
          return CoreService
              .tags('markets', $query);
      }
      $scope.marketsList = marketsList;

      function expertiseList($query) {
          return CoreService
              .tags('expertises', $query);
      }
      $scope.expertiseList = expertiseList;

      AuthService
          .auth(me.identities[0].provider, me)
          .success(function(data, status) {
              CoreService
                  .readOne('startup', me.id, $state.params.profileId)
                  .then(onReadOneSuccess, onReadOneError);
          });
  }

  function onNotAuthenticated() {
      $log.debug('profile-edit-startup: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})

.controller('ProfileInvestorCtrl', function($scope, $state, $log, $ionicModal, $ionicPopup, CoreService, AuthService, $location, STAGE, $upload, Profile) {
  $scope.profile = {};

  function onAuthenticated(me) {
      $log.debug('profile-single: is authenticated');
      $scope.currentUser = me;
      $scope.model.currentUser = me;
      $scope.editMode = ($location.search().edit === 'true') || Profile.is_my_profile($state.params.profileId);

      function getFavoriteStageNameHandler() {
          var out = {};

          if ($scope.profile.lookingForIdea) {
              out.index = 0;
              out.label = 'Ideia';
          } else if ($scope.profile.lookingForProtoProgress) {
              out.index = 1;
              out.label = 'Prototipagem';
          } else if ($scope.profile.lookingForProtoDone) {
              out.index = 2;
              out.label = 'Protótipo pronto';
          } else if ($scope.profile.lookingForStartupLaunched) {
              out.index = 3;
              out.label = 'Lançada';
          } else if ($scope.profile.lookingForProfitingStartup) {
              out.index = 4;
              out.label = 'Com receita';
          }

          return out;
      }
      $scope.getFavoriteStageName = getFavoriteStageNameHandler;

      function sendMessageHandler(profile) {
          $log.debug('Send message to ' + profile.title);
          $ionicModal
              .fromTemplateUrl('templates/send-message.html', {
                  scope: $scope
              })
              .then(function(modal) {
                  $scope.modal = modal;
                  $scope.modal.show();

                  $scope.send = function() {
                      $scope.modal.hide();
                  };
              });
      }
      $scope.sendMessage = sendMessageHandler;

      function recommendHandler(profile) {
          $log.debug('Recommend ' + profile.title);

          function onRecommendSuccess(data) {
              $log.debug('on success handler');
              if (!data.err) {
                  $log.debug(profile.name + ' was recommended!');
                  $scope.hasRecommend = true;
                  $scope.recommendationCounter += 1;
              }
          }

          function onRecommendError(data, status) {
              $log.debug('on error handler');
              $log.debug('Something goes wrong: ' + data);
          }

          $log.debug('Profile ' + profile.name + ' recommended!');
          CoreService
              .recommend(me.id, {
                  recommend: profile.profile_id._id || profile.profile_id,
                  type: 'investor'
              })
              .then(onRecommendSuccess, onRecommendError);
      }
      $scope.recommend = recommendHandler;

      function unrecommendHandler(profile) {
          $log.debug('unrecommend ' + profile.name);

          function onUnrecommendSuccess(data) {
              $log.debug('on success handler');
              if (!data.err) {
                  $log.debug(profile.name + ' was unrecommend!');
                  $scope.hasRecommend = false;
                  $scope.recommendationCounter -= 1;
              }
          }

          function onUnrecommendError(data, status) {
              $log.debug('on error handler');
              $log.debug('Something goes wrong: ' + data);
          }

          CoreService
              .unrecommend(me.id, {
                  recommend: profile.profile_id._id || profile.profile_id,
                  type: 'investor'
              })
              .then(onUnrecommendSuccess, onUnrecommendError);
      }
      $scope.unrecommend = unrecommendHandler;

      function favoriteHandler(profile) {
          $log.debug('favorite ' + profile.name);

          function onFavoriteSuccess(data) {
              $log.debug('on success handler');
              if (!data.err) {
                  $log.debug(profile.name + ' was favorited!');
                  $scope.hasFavorite = true;
              }
          }

          function onFavoriteError(data, status) {
              $log.debug('on error handler');
              $log.debug('Something goes wrong: ' + data);
          }

          CoreService
              .favorite(me.id, {
                  favorite: profile.profile_id._id || profile.profile_id,
                  type: 'investor'
              })
              .then(onFavoriteSuccess, onFavoriteError);
      }
      $scope.favorite = favoriteHandler;

      function unfavoriteHandler(profile) {
          $log.debug('unfavorite ' + profile.name);

          function onUnfavoriteSuccess(data) {
              $log.debug('on success handler');
              if (!data.err) {
                  $log.debug(profile.name + ' was unfavorited!');
                  $scope.hasFavorite = false;
              }
          }

          function onUnfavoriteError(data, status) {
              $log.debug('on error handler');
              $log.debug('Something goes wrong: ' + data);
          }

          CoreService
              .unfavorite(me.id, {
                  favorite: profile.profile_id._id || profile.profile_id,
                  type: 'investor'
              })
              .then(onUnfavoriteSuccess, onUnfavoriteError);
      }
      $scope.unfavorite = unfavoriteHandler;

      function onHasFavoriteSuccess(data) {
          $log.debug('on success handler');
          if (!data.err) {
              $scope.hasFavorite = data.result;
          }
      }

      function onHasFavoriteError(data, status) {
          $log.debug('on error handler');
          $log.debug('Something goes wrong: ' + data);
      }

      function onHasRecommendSuccess(data) {
          $log.debug('on success handler');
          if (!data.err) {
              $scope.hasRecommend = data.result;
          }
      }

      function onHasRecommendError(data, status) {
          $log.debug('on error handler');
          $log.debug('Something goes wrong: ' + data);
      }

      function onRecommendCounterSuccess(data) {
          $log.debug('on success handler');
          if (!data.err) {
              $scope.recommendationCounter = data.result;
          }
      }

      function onRecommendCounterError(data, status) {
          $log.debug('on error handler');
          $log.debug('Something goes wrong: ' + data);
      }

      function onReadOneSuccess(data) {
          $log.debug('investor on success handler');
          $scope.profile = data.result;
          $scope.profile.type = 'investor';

          CoreService
              .hasFavorite(me.id, $scope.profile.profile_id._id || $scope.profile.profile_id)
              .then(onHasFavoriteSuccess, onHasFavoriteError);

          CoreService
              .hasRecommend(me.id, $scope.profile.profile_id._id || $scope.profile.profile_id)
              .then(onHasRecommendSuccess, onHasRecommendError);

          CoreService
              .recommendCounter(me.id, $scope.profile.profile_id || $scope.profile.profile_id)
              .then(onRecommendCounterSuccess, onRecommendCounterError);
      }

      function onReadOneError(data, status) {
          $log.debug('on error handler');
          $log.debug('Something goes wrong: ' + data);
      }

      AuthService
          .auth(me.identities[0].provider, me)
          .success(function(data, status) {
              CoreService
                  .readOne('investor', me.id, $state.params.profileId)
                  .then(onReadOneSuccess, onReadOneError);
          });
  }

  function onNotAuthenticated() {
      $log.debug('profile-single: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})

.controller('ProfileEditInvestorCtrl', function($scope, $state, $log, CoreService, AuthService, $location, $upload, STAGE, $ionicNavBarDelegate) {
  $scope.profile = {};
  $scope.profile.investor = {};

  function onAuthenticated(me) {
      $log.debug('profile-edit-startup: is authenticated');
      $scope.currentUser = me;
      $scope.model.currentUser = me;

      function onReadOneSuccess(data, status) {
          $log.debug('on read one success');
          $('#investimentRangeStealthMode').prop('checked', data.result.investimentRangeStealthMode);
          $('#lookingForIdea').prop('checked', data.result.lookingForIdea);
          $('#lookingForProtoProgress').prop('checked', data.result.lookingForProtoProgress);
          $('#lookingForProtoDone').prop('checked', data.result.lookingForProtoDone);
          $('#lookingForStartupLaunched').prop('checked', data.result.lookingForStartupLaunched);
          $('#lookingForProfitingStartup').prop('checked', data.result.lookingForProfitingStartup);
          $scope.profile.investor = data.result;
      }

      function onReadOneError(data, status) {
          $log.debug('on read one error');
          $log.debug('Something goes wrong: ' + data);
      }

      $scope.goBack = function() {
          $ionicNavBarDelegate.back();
      };

      function editHandler(entity) {
          function onSuccess(data) {
              if (!data.err && data.status) {
                  $scope.goBack();
              }
          }

          function onError(data, status) {
              $log.debug('Something goes wrong: ' + data);
          }

          CoreService
              .edit(entity, {
                  avatar: $scope.profile.investor.avatar,
                  name: $scope.profile.investor.name,
                  country: $scope.profile.investor.country,
                  city: $scope.profile.investor.city,
                  description: $scope.profile.investor.description,
                  expertise: $scope.profile.investor.expertise,
                  favoriteMarket: $scope.profile.investor.favoriteMarket,
                  minInvestimentRange: $scope.profile.investor.minInvestimentRange,
                  maxInvestimentRange: $scope.profile.investor.maxInvestimentRange,
                  investimentRangeStealthMode: $scope.profile.investor.investimentRangeStealthMode,
                  lookingForIdea: $scope.profile.investor.lookingForIdea,
                  lookingForProtoProgress: $scope.profile.investor.lookingForProtoProgress,
                  lookingForProtoDone: $scope.profile.investor.lookingForProtoDone,
                  lookingForStartupLaunched: $scope.profile.investor.lookingForStartupLaunched,
                  lookingForProfitingStartup: $scope.profile.investor.lookingForProfitingStartup
              }, $scope.currentUser.id)
              .then(onSuccess, onError);
      }
      $scope.edit = editHandler;

      function setPercentageHandler(percentage) {
          $scope.percentage = percentage;
      }
      $scope.setPercentage = setPercentageHandler;

      function getPercentageHandler() {
          return $scope.percentage;
      }
      $scope.getPercentage = getPercentageHandler;

      function setInvestorAvatarHandler(avatar) {
          $scope.profile.investor.avatar = avatar;
      }
      $scope.setInvestorAvatar = setInvestorAvatarHandler;

      function onFileProgress(e) {
          var percentage = parseInt(100.0 * e.loaded / e.total);
          $log.debug('precent: ' + percentage + '%');
          setPercentageHandler(percentage);

          document.getElementById('progressBar').style.display = 'block';

          $scope.barProgress = percentage;
      }

      function onFileSuccess(data) {
          document.getElementById('progressBar').style.display = 'none';
          $log.debug('on file success');
          $log.debug(data);
          var avatarURL = 'http://' + STAGE + '/api/avatar/' + data.result._id;

          setInvestorAvatarHandler(avatarURL);
          setPercentageHandler(0);
      }

      function onFileError() {
          $log.debug('on file error');
      }

      function onFileSelect($files, currentType) {
          $log.debug('on file select');
          $log.debug($files);
          for (var i = 0; i < $files.length; i++) {
              var file = $files[i];
              $scope
                  .upload =
                  $upload
                  .upload({
                      url: 'http://' + STAGE + '/api/avatar',
                      file: file,
                      fileFormDataName: 'image'
                  })
                  .progress(onFileProgress)
                  .success(onFileSuccess)
                  .error(onFileError);
          }
      }
      $scope.onFileSelect = onFileSelect;


      $scope.data = {
          "countries": [],
          "cities": []
      };
      //country autocompelte
      function searchCountry() {
          if (
                  $scope.profile.investor.country != undefined 
              &&  $scope.profile.investor.country.length >= 3
          ) {
              var countries = CoreService
                  .tags('countries', $scope.profile.investor.country);
              countries.then(function(c) {
                  $scope.data.countries = c;
              });
          }
      }
      $scope.searchCountry = searchCountry;

      function onClickACCountry(i) {
          $scope.profile.investor.country = $scope.data.countries[i];
          $scope.data.countries = [];
      }
      $scope.onClickACCountry = onClickACCountry;

      //city autocomplete
      var searchCityTimer = null;
      function searchCity() {
          $scope.data.cities = [];
          if (
                  $scope.profile.investor.city != undefined 
              &&  $scope.profile.investor.city.length >= 3
          ) {
              if(searchCityTimer != null) clearTimeout(searchCityTimer);
              searchCityTimer = setTimeout(function() {
                  console.log("here");
                  console.log($scope.profile.investor.city);
                  var cities = CoreService
                      .tags('cities', $scope.profile.investor.city);
                  cities.then(function(c) {
                      $scope.data.cities = c;
                  });
              }, 500);                
          }
      }        
      $scope.searchCity = searchCity;

      function onClickACCity(i) {
          $scope.profile.investor.city = $scope.data.cities[i];
          $scope.data.cities = [];
      }
      $scope.onClickACCity = onClickACCity;

      // onFileUpload($files, currentType, STAGE, $upload, setPercentageHandler, setAvatarHandler, setBarProgress);

      //autocomplete
      function marketsList($query) {
          return CoreService
              .tags('markets', $query);
      }
      $scope.marketsList = marketsList;

      function expertiseList($query) {
          return CoreService
              .tags('expertises', $query);
      }
      $scope.expertiseList = expertiseList;

      AuthService
          .auth(me.identities[0].provider, me)
          .success(function(data, status) {
              CoreService
                  .readOne('investor', me.id, $state.params.profileId)
                  .then(onReadOneSuccess, onReadOneError);
          });
  }

  function onNotAuthenticated() {
      $log.debug('profile-investor-startup: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})

.controller('ProfileProfessionalCtrl', function($scope, $state, $log, $ionicModal, $ionicPopup, CoreService, AuthService, $location, STAGE, $upload, Profile) {
  $scope.profile = {};

  function onAuthenticated(me) {
      $log.debug('profile-single: is authenticated');
      $scope.currentUser = me;
      $scope.model.currentUser = me;
      $scope.editMode = ($location.search().edit === 'true') || Profile.is_my_profile($state.params.profileId);

      function sendMessageHandler(profile) {
          $log.debug('Send message to ' + profile.title);
          $ionicModal
              .fromTemplateUrl('templates/send-message.html', {
                  scope: $scope
              })
              .then(function(modal) {
                  $scope.modal = modal;
                  $scope.modal.show();

                  $scope.send = function() {
                      $scope.modal.hide();
                  };
              });
      }
      $scope.sendMessage = sendMessageHandler;

      function recommendHandler(profile) {
          $log.debug('Recommend ' + profile.title);

          function onRecommendSuccess(data) {
              $log.debug('on success handler');
              if (!data.err) {
                  $log.debug(profile.name + ' was recommended!');
                  $scope.hasRecommend = true;
                  $scope.recommendationCounter += 1;
              }
          }

          function onRecommendError(data, status) {
              $log.debug('on error handler');
              $log.debug('Something goes wrong: ' + data);
          }

          $log.debug('Profile ' + profile.name + ' recommended!');
          CoreService
              .recommend(me.id, {
                  recommend: profile.profile_id._id || profile.profile_id,
                  type: 'pro'
              })
              .then(onRecommendSuccess, onRecommendError);
      }
      $scope.recommend = recommendHandler;

      function unrecommendHandler(profile) {
          $log.debug('unrecommend ' + profile.name);

          function onUnrecommendSuccess(data) {
              $log.debug('on success handler');
              if (!data.err) {
                  $log.debug(profile.name + ' was unrecommend!');
                  $scope.hasRecommend = false;
                  $scope.recommendationCounter -= 1;
              }
          }

          function onUnrecommendError(data, status) {
              $log.debug('on error handler');
              $log.debug('Something goes wrong: ' + data);
          }

          CoreService
              .unrecommend(me.id, {
                  recommend: profile.profile_id._id || profile.profile_id,
                  type: 'pro'
              })
              .then(onUnrecommendSuccess, onUnrecommendError);
      }
      $scope.unrecommend = unrecommendHandler;

      function favoriteHandler(profile) {
          $log.debug('favorite ' + profile.name);

          function onFavoriteSuccess(data) {
              $log.debug('on success handler');
              if (!data.err) {
                  $log.debug(profile.name + ' was favorited!');
                  $scope.hasFavorite = true;
              }
          }

          function onFavoriteError(data, status) {
              $log.debug('on error handler');
              $log.debug('Something goes wrong: ' + data);
          }

          CoreService
              .favorite(me.id, {
                  favorite: profile.profile_id._id || profile.profile_id,
                  type: 'pro'
              })
              .then(onFavoriteSuccess, onFavoriteError);
      }
      $scope.favorite = favoriteHandler;

      function unfavoriteHandler(profile) {
          $log.debug('unfavorite ' + profile.name);

          function onUnfavoriteSuccess(data) {
              $log.debug('on success handler');
              if (!data.err) {
                  $log.debug(profile.name + ' was unfavorited!');
                  $scope.hasFavorite = false;
              }
          }

          function onUnfavoriteError(data, status) {
              $log.debug('on error handler');
              $log.debug('Something goes wrong: ' + data);
          }

          CoreService
              .unfavorite(me.id, {
                  favorite: profile.profile_id._id || profile.profile_id,
                  type: 'pro'
              })
              .then(onUnfavoriteSuccess, onUnfavoriteError);
      }
      $scope.unfavorite = unfavoriteHandler;

      function onHasFavoriteSuccess(data) {
          $log.debug('on success handler');
          if (!data.err) {
              $scope.hasFavorite = data.result;
          }
      }

      function onHasFavoriteError(data, status) {
          $log.debug('on error handler');
          $log.debug('Something goes wrong: ' + data);
      }

      function onHasRecommendSuccess(data) {
          $log.debug('on success handler');
          if (!data.err) {
              $scope.hasRecommend = data.result;
          }
      }

      function onHasRecommendError(data, status) {
          $log.debug('on error handler');
          $log.debug('Something goes wrong: ' + data);
      }

      function onRecommendCounterSuccess(data) {
          $log.debug('on success handler');
          if (!data.err) {
              $scope.recommendationCounter = data.result;
          }
      }

      function onRecommendCounterError(data, status) {
          $log.debug('on error handler');
          $log.debug('Something goes wrong: ' + data);
      }

      function onReadOneSuccess(data) {
          $log.debug('on success handler');
          if (!data.err) {
              console.log('DATARESULT');
              $scope.profile = data.result;
              $scope.profile.type = 'professional';
              $log.debug($scope.profile.type);
              console.log($scope.profile);
              CoreService
                  .hasFavorite(me.id, $scope.profile.profile_id._id || $scope.profile.profile_id)
                  .then(onHasFavoriteSuccess, onHasFavoriteError);

              CoreService
                  .hasRecommend(me.id, $scope.profile.profile_id._id || $scope.profile.profile_id)
                  .then(onHasRecommendSuccess, onHasRecommendError);

              CoreService
                  .recommendCounter(me.id, $scope.profile.profile_id._id || $scope.profile.profile_id)
                  .then(onRecommendCounterSuccess, onRecommendCounterError);
          }
      }

      $scope.testeid = $state.params.profileId;

      function onReadOneError(data, status) {
          $log.debug('on error handler');
          $log.debug('Something goes wrong: ' + data);
      }

      AuthService
          .auth(me.identities[0].provider, me)
          .success(function(data, status) {
              CoreService
                  .readOne('professional', me.id, $state.params.profileId)
                  .then(onReadOneSuccess, onReadOneError);
          });
  }

  function onNotAuthenticated() {
      $log.debug('profile-single: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})

.controller('ProfileEditProfessionalCtrl', function($scope, $state, $log, CoreService, AuthService, $location, $upload, STAGE, $ionicNavBarDelegate) {
  $scope.profile = {};
  $scope.profile.pro = {};

  function onAuthenticated(me) {
      $log.debug('profile-edit-startup: is authenticated');
      $scope.currentUser = me;
      $scope.model.currentUser = me;

      function onReadOneSuccess(data, status) {
          $log.debug('on read one success');
          $('#lookingForEquity').prop('checked', data.result.lookingForEquity);
          $('#lookingForCash').prop('checked', data.result.lookingForCash);
          $scope.profile.pro = data.result;
      }

      function onReadOneError(data, status) {
          $log.debug('on read one error');
          $log.debug('Something goes wrong: ' + data);
      }

      $scope.goBack = function() {
          $ionicNavBarDelegate.back();
      };

      function editHandler(entity) {
          function onSuccess(data) {
              if (!data.err && data.status) {
                  $scope.goBack();
              }
          }

          function onError(data, status) {
              $log.debug('Something goes wrong: ' + data);
          }

          CoreService
              .edit(entity, {
                  avatar: $scope.profile.pro.avatar,
                  name: $scope.profile.pro.name,
                  site: $scope.profile.pro.site,
                  country: $scope.profile.pro.country,
                  city: $scope.profile.pro.city,
                  expertise: $scope.profile.pro.expertise,
                  description: $scope.profile.pro.description,
                  lookingForEquity: $scope.profile.pro.lookingForEquity,
                  lookingForCash: $scope.profile.pro.lookingForCash
              }, $scope.currentUser.id)
              .then(onSuccess, onError);
      }
      $scope.edit = editHandler;

      function setPercentageHandler(percentage) {
          $scope.percentage = percentage;
      }
      $scope.setPercentage = setPercentageHandler;

      function getPercentageHandler() {
          return $scope.percentage;
      }
      $scope.getPercentage = getPercentageHandler;

      function setProAvatarHandler(avatar) {
          $scope.profile.pro.avatar = avatar;
      }
      $scope.setProAvatar = setProAvatarHandler;

      function onFileProgress(e) {
          var percentage = parseInt(100.0 * e.loaded / e.total);
          $log.debug('precent: ' + percentage + '%');
          setPercentageHandler(percentage);

          document.getElementById('progressBar').style.display = 'block';

          $scope.barProgress = percentage;
      }

      function onFileSuccess(data) {
          document.getElementById('progressBar').style.display = 'none';
          $log.debug('on file success');
          $log.debug(data);
          var avatarURL = 'http://' + STAGE + '/api/avatar/' + data.result._id;

          setProAvatarHandler(avatarURL);
          setPercentageHandler(0);
      }

      function onFileError() {
          $log.debug('on file error');
      }

      function onFileSelect($files, currentType) {
          $log.debug('on file select');
          $log.debug($files);
          for (var i = 0; i < $files.length; i++) {
              var file = $files[i];
              $scope
                  .upload =
                  $upload
                  .upload({
                      url: 'http://' + STAGE + '/api/avatar',
                      file: file,
                      fileFormDataName: 'image'
                  })
                  .progress(onFileProgress)
                  .success(onFileSuccess)
                  .error(onFileError);
          }
      }
      $scope.onFileSelect = onFileSelect;

      $scope.data = {
          "countries": [],
          "cities": []
      };
      //country autocompelte
      function searchCountry() {
          if (
                  $scope.profile.pro.country != undefined 
              &&  $scope.profile.pro.country.length >= 3
          ) {
              var countries = CoreService
                  .tags('countries', $scope.profile.pro.country);
              countries.then(function(c) {
                  $scope.data.countries = c;
              });
          }
      }
      $scope.searchCountry = searchCountry;

      function onClickACCountry(i) {
          $scope.profile.pro.country = $scope.data.countries[i];
          $scope.data.countries = [];
      }
      $scope.onClickACCountry = onClickACCountry;

      //city autocomplete
      var searchCityTimer = null;
      function searchCity() {
          $scope.data.cities = [];
          if (
                  $scope.profile.pro.city != undefined 
              &&  $scope.profile.pro.city.length >= 3
          ) {
              if(searchCityTimer != null) clearTimeout(searchCityTimer);
              searchCityTimer = setTimeout(function() {
                  console.log("here");
                  console.log($scope.profile.pro.city);
                  var cities = CoreService
                      .tags('cities', $scope.profile.pro.city);
                  cities.then(function(c) {
                      $scope.data.cities = c;
                  });
              }, 500);                
          }
      }        
      $scope.searchCity = searchCity;

      function onClickACCity(i) {
          $scope.profile.pro.city = $scope.data.cities[i];
          $scope.data.cities = [];
      }
      $scope.onClickACCity = onClickACCity;

      // onFileUpload($files, currentType, STAGE, $upload, setPercentageHandler, setAvatarHandler, setBarProgress);

      //autocomplete
      function expertiseList($query) {
          return CoreService
              .tags('expertises', $query);
      }
      $scope.expertiseList = expertiseList;

      AuthService
          .auth(me.identities[0].provider, me)
          .success(function(data, status) {
              CoreService
                  .readOne('professional', me.id, $state.params.profileId)
                  .then(onReadOneSuccess, onReadOneError);
          });
  }

  function onNotAuthenticated() {
      $log.debug('profile-edit-startup: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})

.controller('ProfileStartupCtrl', function($scope, $state, $log, $ionicModal, $ionicPopup, CoreService, AuthService, $location, STAGE, $upload, StartupManager, Profile) {
    $scope.profile = {};
    $scope.hasFavorite = false;

    function onAuthenticated(me) {
        $log.debug('profile-single: is authenticated');
        $scope.currentUser = me;
        $scope.model.currentUser = me;
        $scope.editMode = ($location.search().edit === 'true') || Profile.is_my_profile($state.params.profileId);

        function getStageNameHandler() {
            var out = {};

            out.index = $scope.profile.developmentStage;

            switch ($scope.profile.developmentStage) {
                case 1:
                    out.label = 'Prototipagem';
                    break;
                case 2:
                    out.label = 'Protótipo pronto';
                    break;
                case 3:
                    out.label = 'Lançada';
                    break;
                case 4:
                    out.label = 'Com receita';
                    break;
                default:
                    out.label = 'Ideia';
                    break;
            }

            return out;
        }
        $scope.getStageName = getStageNameHandler;

        function hasSearchingForHandler(profile) {
            var hasLookingForPros = profile.lookingForPros;
            var hasLookingForExpertise = (profile.lookingForExpertise && (profile.lookingForExpertise.length));
            var hasOfferEquity = profile.offerEquity;
            var hasOfferCash = profile.offerCash;
            var hasSearchingFor = (hasLookingForPros || (hasLookingForExpertise || (hasOfferEquity || (hasOfferCash))));

            return hasSearchingFor;
        }
        $scope.hasSearchingFor = hasSearchingForHandler;


        function hasExpertisesSettingHandler(profile) {
            var hasLookingForPros = profile.lookingForPros;
            var hasLookingForExpertise = (profile.lookingForExpertise && (profile.lookingForExpertise.length));
            var hasOfferEquity = profile.offerEquity;
            var hasOfferCash = profile.offerCash;
            var hasExpertisesSetting = (hasLookingForPros || (hasLookingForExpertise || (hasOfferEquity || (hasOfferCash))));

            return hasExpertisesSetting;
        }
        $scope.hasExpertisesSetting = hasExpertisesSettingHandler;

        function recommendHandler(profile) {
            $log.debug('Recommend ' + profile.title);

            function onRecommendSuccess(data) {
                $log.debug('on success handler');
                if (!data.err) {
                    $log.debug(profile.name + ' was recommended!');
                    $scope.hasRecommend = true;
                    $scope.recommendationCounter += 1;
                }
            }

            function onRecommendError(data, status) {
                $log.debug('on error handler');
                $log.debug('Something goes wrong: ' + data);
            }

            $log.debug('Profile ' + profile.name + ' recommended!');
            CoreService
                .recommend(me.id, {
                    recommend: profile.profile_id._id || profile.profile_id,
                    type: 'startup'
                })
                .then(onRecommendSuccess, onRecommendError);
        }
        $scope.recommend = recommendHandler;

        function unrecommendHandler(profile) {
            $log.debug('unrecommend ' + profile.name);

            function onUnrecommendSuccess(data) {
                $log.debug('on success handler');
                if (!data.err) {
                    $log.debug(profile.name + ' was unrecommend!');
                    $scope.hasRecommend = false;
                    $scope.recommendationCounter -= 1;
                }
            }

            function onUnrecommendError(data, status) {
                $log.debug('on error handler');
                $log.debug('Something goes wrong: ' + data);
            }

            CoreService
                .unrecommend(me.id, {
                    recommend: profile.profile_id._id || profile.profile_id,
                    type: 'startup'
                })
                .then(onUnrecommendSuccess, onUnrecommendError);
        }
        $scope.unrecommend = unrecommendHandler;

        function favoriteHandler(profile) {
            $log.debug('favorite ' + profile.name);

            function onFavoriteSuccess(data) {
                $log.debug('on success handler');
                if (!data.err) {
                    $log.debug(profile.name + ' was favorited!');
                    $scope.hasFavorite = true;
                }
            }

            function onFavoriteError(data, status) {
                $log.debug('on error handler');
                $log.debug('Something goes wrong: ' + data);
            }

            CoreService
                .favorite(me.id, {
                    favorite: profile.profile_id._id || profile.profile_id,
                    type: 'startup'
                })
                .then(onFavoriteSuccess, onFavoriteError);
        }
        $scope.favorite = favoriteHandler;

        function unfavoriteHandler(profile) {
            $log.debug('unfavorite ' + profile.name);

            function onUnfavoriteSuccess(data) {
                $log.debug('on success handler');
                if (!data.err) {
                    $log.debug(profile.name + ' was unfavorited!');
                    $scope.hasFavorite = false;
                }
            }

            function onUnfavoriteError(data, status) {
                $log.debug('on error handler');
                $log.debug('Something goes wrong: ' + data);
            }

            CoreService
                .unfavorite(me.id, {
                    favorite: profile.profile_id._id || profile.profile_id,
                    type: 'startup'
                })
                .then(onUnfavoriteSuccess, onUnfavoriteError);
        }
        $scope.unfavorite = unfavoriteHandler;

        function getProfileInformationHandler(profile) {
            var provider = null;

            if (profile.profile_id && profile.profile_id.user) {
                if (profile.profile_id.user.provider.linkedin) {
                    provider = profile.profile_id.user.provider.linkedin;
                } else if (profile.profile_id.user.provider.facebook) {
                    provider = profile.profile_id.user.provider.facebook;
                } else if (profile.profile_id.user.provider.google) {
                    provider = profile.profile_id.user.provider.google;
                } else if (profile.profile_id.user.provider.github) {
                    provider = profile.profile_id.user.provider.github;
                }
            }

            return provider;
        }
        $scope.getProfileInformation = getProfileInformationHandler;

        function onHasFavoriteSuccess(data) {
            $log.debug('on success handler');
            if (!data.err) {
                $scope.hasFavorite = data.result;
            }
        }

        function onHasFavoriteError(data, status) {
            $log.debug('on error handler');
            $log.debug('Something goes wrong: ' + data);
        }

        function onHasRecommendSuccess(data) {
            $log.debug('on success handler');
            if (!data.err) {
                $scope.hasRecommend = data.result;
            }
        }

        function onHasRecommendError(data, status) {
            $log.debug('on error handler');
            $log.debug('Something goes wrong: ' + data);
        }

        function onRecommendCounterSuccess(data) {
            $log.debug('on success handler');
            if (!data.err) {
                $scope.recommendationCounter = data.result;
            }
        }

        function onRecommendCounterError(data, status) {
            $log.debug('on error handler');
            $log.debug('Something goes wrong: ' + data);
        }


        function onReadOneSuccess(data) {
            $log.debug('on success handler');
            if (!data.err) {
                $scope.profile = data.result;
                $scope.profile.type = 'startup';
                $log.debug($scope.profile.type);

                CoreService
                    .hasFavorite(me.id, $scope.profile.profile_id._id || $scope.profile.profile_id)
                    .then(onHasFavoriteSuccess, onHasFavoriteError);

                CoreService
                    .hasRecommend(me.id, $scope.profile.profile_id._id || $scope.profile.profile_id)
                    .then(onHasRecommendSuccess, onHasRecommendError);

                CoreService
                    .recommendCounter(me.id, $scope.profile.profile_id._id || $scope.profile.profile_id)
                    .then(onRecommendCounterSuccess, onRecommendCounterError);

                // StartupManager.getMembers($scope.profile, me.id).then(
                //     function(members) {
                //         $scope.members = members;
                //     }
                // );

                // StartupManager.getInvestors($scope.profile, me.id).then(
                //     function(investors) {
                //         $scope.investors = investors;
                //     }
                // );
            }
        }

        function onReadOneError(data, status) {
            $log.debug('on error handler');
            $log.debug('Something goes wrong: ' + data);
        }

        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {
                CoreService
                    .readOne('startup', me.id, $state.params.profileId)
                    .then(onReadOneSuccess, onReadOneError);
            });
    }

    function onNotAuthenticated() {
        $log.debug('profile-single: is not authenticated');
        $state.go('app.intro');
    }

    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(window.Hull.currentUser());
            } else {
                onNotAuthenticated();
            }
        });
    }
})

.controller('RegisterCtrl', function($scope, $state, $ionicModal, $ionicPopup, AuthService, CoreService, STAGE, $http, $log, $upload, $stateParams) {
    $scope.data = {
        "countries": [],
        "cities": [],
        type: ""
    };

    $scope.percentage = 0;
    $scope.profile = {
        pro: {
            avatar: ''
        },
        startup: {
            avatar: '',
            team: [],
        },
        investor: {
            avatar: ''
        },
        tmp: {}
    };
    $scope.profiles = [{
        title: 'Startup',
        name: 'startup',
        full_name: 'startup',
        icon: 'img/startup-circle-icon.png',
        checked: false
    }, {
        title: 'Pro',
        name: 'pro',
        full_name: 'professional',
        icon: 'img/pro-circle-icon.png',
        checked: false
    }, {
        title: 'Investidor',
        name: 'investor',
        full_name: 'investor',
        icon: 'img/investor-circle-icon.png',
        checked: false
    }, ];

    $scope.profileAction = $stateParams.profileAction;

    function saveHandler(entity) {
        var hasAvatar = $scope.profile[entity].avatar;
        if (!hasAvatar && ($scope.model.currentUser.picture.indexOf('graph.facebook') > -1)) {
            $scope.profile[entity].avatar = $scope.model.currentUser.picture + '?type=large';
        } else if (!hasAvatar) {
            $scope.profile[entity].avatar = $scope.model.currentUser.picture;
        }

        function onSuccess(data) {
            if (!data.err) {
                if (entity == 'startup') {
                    $scope.profile.pro.site = $scope.profile['startup'].site;
                    $scope.profile.pro.city = $scope.profile['startup'].city;
                    $scope.profile.pro.country = $scope.profile['startup'].country;

                    $scope.modal.remove();
                    $scope.enableState(1, 'pro');
                } else {
                    $state.go('app.dashboard');
                    $scope.modal.remove();
                }
            }
        }

        function onError(data, status) {
            $log.debug('Something goes wrong: ' + data);
        }
        if ($scope.profile.isValid) {
            CoreService
                .create(entity, $scope.profile[entity], $scope.currentUser.id)
                .then(onSuccess, onError);
        } else if ($scope.profile.isInvalid) {
            $log.debug('Form invalid!');
        }
    }
    $scope.save = saveHandler;

    function setPercentageHandler(percentage) {
        $scope.percentage = percentage;
    }
    $scope.setPercentage = setPercentageHandler;

    function getPercentageHandler() {
        return $scope.percentage;
    }
    $scope.getPercentage = getPercentageHandler;

    function setStartupAvatarHandler(avatar) {
        $scope.profile.startup.avatar = avatar;
    }
    $scope.setStartupAvatar = setStartupAvatarHandler;

    function setProAvatarHandler(avatar) {
        $scope.profile.pro.avatar = avatar;
    }
    $scope.setProAvatar = setProAvatarHandler;

    function setInvestorAvatarHandler(avatar) {
        $scope.profile.investor.avatar = avatar;
    }
    $scope.setInvestorAvatar = setInvestorAvatarHandler;

    function onFileProgress(e) {
        var percentage = parseInt(100.0 * e.loaded / e.total);
        $log.debug('precent: ' + percentage + '%');
        setPercentageHandler(percentage);

        document.getElementById('progressBar').style.display = 'block';

        $scope.barProgress = percentage;
    }

    function onFileSuccess(data) {
        document.getElementById('progressBar').style.display = 'none';
        $log.debug('on file success');
        $log.debug(data);
        $log.debug(currentType);
        var self = this;
        var currentType = self.currentType;
        var avatarURL = 'http://' + STAGE + '/api/avatar/' + data.result._id;

        if (currentType === 'startup') {
            setStartupAvatarHandler(avatarURL);
            setProAvatarHandler('');
            setInvestorAvatarHandler('');
        } else if (currentType === 'pro') {
            setProAvatarHandler(avatarURL);
            setStartupAvatarHandler('');
            setInvestorAvatarHandler('');
        } else if (currentType === 'investor') {
            setInvestorAvatarHandler(avatarURL);
            setStartupAvatarHandler('');
            setProAvatarHandler('');
        }
        setPercentageHandler(0);
    }

    function onFileError() {
        $log.debug('on file error');
    }

    function onFileSelect($files, currentType) {
        $log.debug('on file select');
        $log.debug($files);
        for (var i = 0; i < $files.length; i++) {
            var file = $files[i];
            $scope
                .upload =
                $upload
                .upload({
                    url: 'http://' + STAGE + '/api/avatar',
                    file: file,
                    fileFormDataName: 'image'
                })
                .progress(onFileProgress)
                .success(onFileSuccess.bind({
                    currentType: currentType
                }))
                .error(onFileError);
        }
    }
    $scope.onFileSelect = onFileSelect;

    function enableState(id, name) {
        $scope.data.type = name;
        var templateLocation = 'templates/settings-' + name + '.html';

        $scope.id = id;

        $ionicModal.fromTemplateUrl(templateLocation, {
            scope: $scope,
            animation: 'slide-in-left'
        }).then(function(modal) {
            $scope.modal = modal;
            modal.show();
        });
    }
    $scope.enableState = enableState;

    function goToDashboard() {
        $state.go('app.dashboard');
    }
    $scope.goToDashboard = goToDashboard;

    function isMemberValid(member) {
        return member.lookingForExpertise.length > 0 && ( member.offerEquity || member.offerCash );
    }

    function addToTeam(member) {
        if( isMemberValid(member) ) {
            $scope.profile.startup.team.push(member);
            $scope.profile.tmp = {};
            $('#offerEquity').prop('checked', false);
            $('#offerCash').prop('checked', false);
        }
    }
    $scope.addToTeam = addToTeam;

    function removeTagFromTeam(parentIndex, i) {
        $scope.profile.startup.team[parentIndex].lookingForExpertise.splice(i, 1);
        if (!$scope.profile.startup.team[parentIndex].lookingForExpertise.length) {
            removeFromTeam(parentIndex);
        }
    }
    $scope.removeTagFromTeam = removeTagFromTeam;

    function removeFromTeam(i) {
        $scope.profile.startup.team.splice(i, 1);
    }

    function onAuthenticated(me) {
        $log.debug('profile: is authenticated');
        $scope.model.currentUser = me;
        $scope.currentUser = me;
        $log.debug(me);

        function onSuccess(data) {
            $log.debug(data);
            var keys = (!data.err && (data.result)) ? Object.keys(data.result) : [];
            var hasStartup = keys.indexOf('startup') > -1;
            var hasPro = keys.indexOf('professional') > -1;
            var hasInvestor = keys.indexOf('investor') > -1;
            var hasOneEntity = (keys ? (hasStartup || (hasPro || (hasInvestor))) : null);

            $scope.isProfileAuthenticated = true;

            if (!data.err && (hasOneEntity)) {

                if ($scope.profileAction == "add") {

                    $scope.profileKeys = Object.keys(data.result);

                } else {
                    if (hasStartup) {

                        CoreService
                            .list('pro', me.id)
                            .then(
                                function(data) {
                                    if (data.result.length == 0) {
                                        $scope.enableState(1, 'pro');
                                    } else {
                                        $state.go('app.dashboard');
                                    }
                                },
                                onError);

                    } else {
                        $state.go('app.dashboard');
                    }
                }
            }
        }

        function onError(data, status) {
            $log.debug('Something goes wrong: ' + data);
        }

        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {
                CoreService
                    .hasEntity(me.id)
                    .then(onSuccess, onError);
            });
    }

    //country autocompelte
    function searchCountry() {
        if (
                    $scope.profile[$scope.data.type].country != undefined 
                &&  $scope.profile[$scope.data.type].country.length >= 3
            ) {
            var countries = CoreService
                .tags('countries', $scope.profile[$scope.data.type].country);
            countries.then(function(c) {
                $scope.data.countries = c;
            });
        }
    }
    $scope.searchCountry = searchCountry;

    function onClickACCountry(i) {
        $scope.profile[$scope.data.type].country = $scope.data.countries[i];
        $scope.data.countries = [];
    }
    $scope.onClickACCountry = onClickACCountry;

    //city autocomplete
    var searchCityTimer = null;
    function searchCity() {
        $scope.data.cities = [];
        if (
                $scope.profile[$scope.data.type].city != undefined 
            &&  $scope.profile[$scope.data.type].city.length >= 3
        ) {
            if(searchCityTimer != null) clearTimeout(searchCityTimer);
            searchCityTimer = setTimeout(function() {
                console.log("here");
                console.log($scope.profile[$scope.data.type].city);
                var cities = CoreService
                    .tags('cities', $scope.profile[$scope.data.type].city);
                cities.then(function(c) {
                    $scope.data.cities = c;
                });
            }, 500);                
        }
    }   
    $scope.searchCity = searchCity;

    function onClickACCity(i) {
        $scope.profile[$scope.data.type].city = $scope.data.cities[i];
        $scope.data.cities = [];
    }
    $scope.onClickACCity = onClickACCity;

    //autocomplete
    function marketsList($query) {
        return CoreService
            .tags('markets', $query);
    }
    $scope.marketsList = marketsList;

    function expertiseList($query) {
        return CoreService
            .tags('expertises', $query);
    }
    $scope.expertiseList = expertiseList;

    function onNotAuthenticated() {
        $log.debug('profile: is not authenticated');
        $state.go('app.intro');
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(me);
            } else {
                onNotAuthenticated();
            }
        });

        window.Hull.on('hull.auth.login', function(me) {
            if (me) {
                onAuthenticated(me);
            } else {
                onNotAuthenticated();
            }
        });
    }
})

.run(function($log) {
  $log.debug('controllers running!');
})

.controller('StartupSearchMemberCtrl', function($scope, $log, $state, $ionicPopup, AuthService, CoreService, StartupManager) {
    function init() {
        $scope.profiles = {};

        $scope.doSearch = function(query_keyword) {
            console.log(query_keyword);
            CoreService.search('pro', $scope.currentUser.id, {
                keyword: query_keyword
            }).then(
                function(data) {
                    $scope.profiles.professional = data.result;
                }
            );

            CoreService.search('investor', $scope.currentUser.id, {
                keyword: query_keyword
            }).then(
                function(data) {
                    $scope.profiles.investor = data.result;
                }
            );
        }

        $scope.inviteMember = function(profile, type) {
            var inviteMemberPopup = $ionicPopup.show({
                title: 'Convidar ' + profile.name,
                template: 'Você deseja convidar o ' + profile.name + ' para sua Startup?',
                buttons: [{
                    text: 'Cancelar'
                }, {
                    text: '<b>Convidar</b>',
                    type: 'button-positive',
                    onTap: function(e) {

                        StartupManager.addMember($scope.profile.startup, profile, type, $scope.currentUser.id).then(
                            function(result) {
                                console.log('result');
                                console.log(result);

                                inviteMemberPopup.close();

                                var alertPopup = $ionicPopup.alert({
                                    title: profile.name + ' foi convidado',
                                    template: 'Aguarde a confirmação do novo membro para sua Startup',
                                });

                            },
                            function(error) {
                                console.log('error');
                                console.log(error);
                            }
                        );

                    }
                }]
            });
        };

    }

    function onAuthSuccess(data, status) {
        CoreService.profile($scope.currentUser.id).then(
            function(data) {
                var keys = (!data.err && (data.result)) ? Object.keys(data.result) : [];
                var hasStartup = keys.indexOf('startup') > -1;

                if (!data.err && (hasStartup)) {
                    $scope.profile = {};
                    $scope.profile.startup = data.result.startup;

                    init();
                }
            }
        );
    }

    function onAuthenticated(me) {
        $log.debug('startup: is authenticated');
        $scope.model.currentUser = me;
        $scope.currentUser = me;

        AuthService
            .auth(me.identities[0].provider, me)
            .success(onAuthSuccess);
    }

    function onNotAuthenticated() {
        $log.debug('startup: is not authenticated');
        $state.go('app.intro');
    }

    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(window.Hull.currentUser());
            } else {
                onNotAuthenticated();
            }
        });
    }
})
