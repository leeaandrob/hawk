angular
  .module('starter.filters', [])
  .filter('getCurrentProfile', function() {
      return function(profile) {
          type = 'startup';
          if ('professional' in profile && profile.professional != null) {
              type = 'professional';
          } else if ('investor' in profile && profile.investor != null) {
              type = 'investor';
          }
          return profile[type];
      };
  })

  .filter('formatNiceDate', function() {
      return function(date) {
          if (date == null)
              date = new Date();
          else
              date = new Date(date);

          months = ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'];
          return date.getDate() + ' de ' + months[date.getMonth()] + ' de ' + date.getFullYear();
      };
  });
