.controller('ContactCtrl', function($scope, $state, $log, AuthService, CoreService) {
  $scope.feedback = {};

  function onAuthenticated(me) {
      $log.debug('about: is authenticated');
      $('.message-thanks').fadeOut(1);

      function onSendFeedbackSuccess(data) {
          $log.debug('on send feedback success');
          $scope.feedback.body = '';
      }

      function onSendFeedbackError(data, status) {
          $log.debug('on send feedback error');
      }

      function send(feedback) {
          CoreService
              .sendFeedback(me.id, feedback)
              .then(onSendFeedbackSuccess, onSendFeedbackError);

          $('.message-thanks').fadeIn(300);

      }
      $scope.send = send;
  }

  function onNotAuthenticated() {
      $log.debug('settings: is not authenticated');
      $state.go('app.intro');
  }
  if (window.Hull.currentUser && window.Hull.currentUser()) {
      onAuthenticated(window.Hull.currentUser());
  } else {
      window.Hull.on('hull.init', function(hull, me) {
          if (me) {
              onAuthenticated(window.Hull.currentUser());
          } else {
              onNotAuthenticated();
          }
      });
  }
})
