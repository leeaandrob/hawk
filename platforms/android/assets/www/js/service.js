angular
  .module('starter.services', ['starter.constant'])

  .factory('AuthService', function($rootScope, $state, $ionicLoading, $http, $window, $log, STAGE) {
    function isLoggedHandler() {
      window.Hull.on('hull.init', function () {
        return window.Hull.currentUser !== undefined ? true : false;
      });
    }

    function isAuthenticatedHandler(onSuccess, onFail) {
      onSuccess = onSuccess || function onSuccessHandler() {};
      onFail = onFail || function onFailHandler() {};

      window.Hull.on('hull.init', function () {
        if (window.Hull.currentUser()) {
          onSuccess(window.Hull.currentUser());
        } else {
          onFail();
        }

        return window.Hull.currentUser();
      });
    }

    function authHandler(provider, me) {
        return $http
                 .post('http://' + STAGE + '/api/auth/' + provider, me);
    }

    function loginHandler(provider) {
      $log.debug('login handler');

      function onLoginSuccess(me) {
        $log.debug('login success');
        $ionicLoading.hide();
      }

      function onLoginError(err) { 
        console.log('Something wrong ' + err.reason);
        $ionicLoading.hide();
      }

      window.Hull 
        .login(provider)
        .then(onLoginSuccess, onLoginError);

      $ionicLoading.show({
        template: 'Entrando...'
      });
    }

    function logoutHandler() {
      $ionicLoading.show({
        template: 'Saindo...'
      });

      function onSuccess() {
        $ionicLoading.hide();
        $state.go('app.intro');
      }

      function onError(err) {
        console.log('Something wrong ' + err.reason);
      }

      window.Hull 
        .logout()
        .then(onSuccess, onError);
    }

    return {
      isLogged: isLoggedHandler,
      isAuthenticated: isAuthenticatedHandler,
      login: loginHandler,
      logout: logoutHandler,
      auth: authHandler,
    };
  })
  .factory('CoreService', function ($rootScope, $q, $state, $http, $window, STAGE) {
    function createHandler(entity, data, accessToken) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status); 
      }

      function onError(data, status) {
        deferred.reject(data, status);
      }

      $http({
          url: 'http://' + STAGE + '/api/' + entity,
          method: 'POST',
          data: data,
          params: { access_token: accessToken }
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function editHandler(entity, data, accessToken) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status); 
      }

      function onError(data, status) {
        deferred.reject(data, status);
      }

      $http({
          url: 'http://' + STAGE + '/api/' + entity,
          method: 'PUT',
          data: data,
          params: { access_token: accessToken }
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function readHandler(entity, accessToken) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/' + entity,
          method: 'GET',
          params: { access_token: accessToken }
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function readOneHandler(entity, accessToken, id) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/' + entity + '/' + id,
          method: 'GET',
          params: { access_token: accessToken }
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function listHandler(entity, accessToken, query) {
      var deferred = $q.defer();

      query = query || {};
      query.access_token = accessToken;

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/' + entity + '/list',
          method: 'GET',
          params: query 
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function searchHandler(entity, accessToken, query) {
      var deferred = $q.defer();

      query = query || {};
      query.access_token = accessToken;

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/' + entity + '/search',
          method: 'GET',
          params: query 
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function hasEntityHandler(accessToken) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/has/profile',
          method: 'GET',
          params: { access_token: accessToken }
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function profileHandler(accessToken) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/profile',
          method: 'GET',
          params: { access_token: accessToken }
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function profileOneHandler(accessToken, id) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/profile/read/' + id,
          method: 'GET',
          params: { access_token: accessToken }
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function favoriteHandler(accessToken, data) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/profile/favorite',
          method: 'POST',
          params: { access_token: accessToken },
          data: data
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function favoriteListHandler(accessToken) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/profile/favorite',
          method: 'GET',
          params: { access_token: accessToken },
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function unfavoriteHandler(accessToken, data) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/profile/unfavorite',
          method: 'PUT',
          params: { access_token: accessToken },
          data: data
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function hasFavoriteHandler(accessToken, favoriteId) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/profile/has/favorite/' + favoriteId,
          method: 'GET',
          params: { access_token: accessToken },
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function recommendHandler(accessToken, data) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/profile/recommend',
          method: 'POST',
          params: { access_token: accessToken },
          data: data
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function recommendCounterHandler(accessToken, recommendId) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/profile/recommend/' + recommendId,
          method: 'GET',
          params: { access_token: accessToken },
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function recommendListHandler(accessToken) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/profile/recommend',
          method: 'GET',
          params: { access_token: accessToken },
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function unrecommendHandler(accessToken, data) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/profile/unrecommend',
          method: 'PUT',
          params: { access_token: accessToken },
          data: data
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function hasRecommendHandler(accessToken, recommendId) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/profile/has/recommend/' + recommendId,
          method: 'GET',
          params: { access_token: accessToken },
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function sendFeedbackHandler(accessToken, feedback) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/feedback',
          method: 'POST',
          params: { access_token: accessToken },
          data: feedback 
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function createConversationHandler(accessToken, data) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/conversation',
          method: 'POST',
          params: { access_token: accessToken },
          data: data, 
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function readConversationHandler(accessToken, conversationID) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/conversation/' + conversationID,
          method: 'GET',
          params: { access_token: accessToken },
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function listConversationHandler(accessToken) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/conversation',
          method: 'GET',
          params: { access_token: accessToken },
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function createConversationMessageHandler(accessToken, conversationID, data) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/conversation/message/' + conversationID,
          method: 'POST',
          data: data,
          params: { access_token: accessToken },
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function blogPostsHandler(accessToken) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/blog/posts',
          method: 'GET',
          params: { access_token: accessToken },
        })
        .success(onSuccess)
        .error(onError);

      return deferred.promise;
    }

    function tagsHandler(type, query) {
      var deferred = $q.defer();

      function onSuccess(data, status) {
        deferred.resolve(data, status);
      }

      function onError(data, status) {
        deferred.reject(data, status); 
      }

      $http({
          url: 'http://' + STAGE + '/api/tags/'+type,
          method: 'GET',
          params: { q: query },
        })
        .success(onSuccess)
        .error(onError);

      //var deferred = $q.defer();
      //deferred.resolve([{ text: 'Tag9' },{ text: 'Tag10' }]);
      return deferred.promise;
    }

    return {
      create: createHandler,
      edit: editHandler,
      read: readHandler,
      readOne: readOneHandler,
      list: listHandler, 
      search: searchHandler, 
      hasEntity: hasEntityHandler,
      profile: profileHandler,
      profileOne: profileOneHandler,
      favorite: favoriteHandler,
      favoriteList: favoriteListHandler,
      unfavorite: unfavoriteHandler,
      hasFavorite: hasFavoriteHandler,
      recommend: recommendHandler,
      recommendCounter: recommendCounterHandler,
      recommendList: recommendListHandler,
      unrecommend: unrecommendHandler,
      hasRecommend: hasRecommendHandler,
      sendFeedback: sendFeedbackHandler,
      createConversation: createConversationHandler,
      listConversation: listConversationHandler,
      readConversation: readConversationHandler,
      createConversationMessage: createConversationMessageHandler,
      blogPosts: blogPostsHandler,
      tags: tagsHandler
    }; 
  })

  .service('$MessageManager', function($q, $http, STAGE) {
    this.sendMessage = function(toId, content, accessToken) {
      var deferred = $q.defer();

      data = {
        to: toId,
        content: content
      };

      $http({
          url: 'http://' + STAGE + '/api/message',
          method: 'POST',
          data: data,
          params: { access_token: accessToken }
        })
        .success(function(data) {
          deferred.resolve(data); 
        })
        .error(function(data) {
          deferred.reject(data);
        });

      return deferred.promise;
    };

    this.getInboxMessages = function(accessToken) {
      var deferred = $q.defer();

      $http({
          url: 'http://' + STAGE + '/api/message/inbox',
          method: 'GET',
          params: { access_token: accessToken }
        })
        .success(function(data) {
          deferred.resolve(data.result); 
        })
        .error(function(data, status) {
          deferred.reject(data, status);
        });

      return deferred.promise;
    };

    this.getConversation = function(otherGuyId, startAt, accessToken) {
      var deferred = $q.defer();
      
      $http({
          url: 'http://' + STAGE + '/api/message/' + otherGuyId,
          method: 'GET',
          params: { access_token: accessToken, start_at: startAt }
        })
        .success(function(data) {
          deferred.resolve(data.result); 
        })
        .error(function(data) {
          deferred.reject(data);
        });

      return deferred.promise;
    };

  })

  .service('StartupManager', function($q, $http, STAGE) {
    this.addMember = function(startup, profile, type, accessToken) {
      var deferred = $q.defer();

      // var startup_id = startup.profile_id;
      // var profile_id = profile.profile_id;
      var startup_id = startup._id;
      var profile_id = profile._id;

      var data = {member: profile_id};

      $http({
          url: 'http://' + STAGE + '/api/startup/member/'+ type +'/' + startup_id,
          method: 'PUT',
          params: { access_token: accessToken },
          data: data
        })
        .success(function(data) {
          deferred.resolve(data.result); 
        })
        .error(function(data, status) {
          deferred.reject(data, status);
        });

      return deferred.promise;

    }

    this.getInvites = function(profile, type, accessToken) {
      var deferred = $q.defer();
      
      // var profile_id = profile.profile_id;
      var profile_id = profile._id;

       $http({
          url: 'http://' + STAGE + '/api/startup/member/'+ type +'/confirm/' + profile_id,
          method: 'GET',
          params: { access_token: accessToken }
        })
        .success(function(data) {
          deferred.resolve(data.result); 
        })
        .error(function(data, status) {
          deferred.reject(data, status);
        });

      return deferred.promise;
    }

    this.confirmMember = function(profile, type, invite, accessToken) {
      var deferred = $q.defer();
      
      var invite_id = invite._id;
      var profile_id = profile._id;

      var data = {member: profile_id};

      $http({
          url: 'http://' + STAGE + '/api/startup/member/'+ type +'/confirm/' + invite_id,
          method: 'PUT',
          params: { access_token: accessToken },
          data: data
        })
        .success(function(data) {
          deferred.resolve(data.result); 
        })
        .error(function(data, status) {
          deferred.reject(data, status);
        });

      return deferred.promise;
    }

    this.refuseMember = function(profile, type, invite, accessToken) {
      var deferred = $q.defer();
      
      var invite_id = invite._id;
      var profile_id = profile._id;

      var data = {member: profile_id};

      $http({
          url: 'http://' + STAGE + '/api/startup/member/'+ type +'/refused/' + invite_id,
          method: 'PUT',
          params: { access_token: accessToken },
          data: data
        })
        .success(function(data) {
          deferred.resolve(data.result); 
        })
        .error(function(data, status) {
          deferred.reject(data, status);
        });

      return deferred.promise;
    }

    this.getMembers = function(startup, accessToken) {
      var deferred = $q.defer();

      var startup_id = startup._id;

      $http({
          url: 'http://' + STAGE + '/api/startup/member/confirm/' + startup_id,
          method: 'GET',
          params: { confirmed: true, access_token: accessToken },
        })
        .success(function(data) {
          deferred.resolve(data.result); 
        })
        .error(function(data, status) {
          deferred.reject(data, status);
        });

      return deferred.promise;
    }

    this.getInvestors = function(startup, accessToken) {
      var deferred = $q.defer();

      var startup_id = startup._id;

      $http({
          url: 'http://' + STAGE + '/api/startup/member/investor/confirm/' + startup_id,
          method: 'GET',
          params: { confirmed: true, access_token: accessToken },
        })
        .success(function(data) {
          deferred.resolve(data.result); 
        })
        .error(function(data, status) {
          deferred.reject(data, status);
        });

      return deferred.promise;
    }

  })

  .service('Profile', function() {
    var profile_logged = null;

    this.is_my_profile = function(profile_id) {
      return (profile_logged.startup && profile_logged.startup._id == profile_id) ||
             (profile_logged.professional && profile_logged.professional._id == profile_id) ||
             (profile_logged.investor && profile_logged.investor._id == profile_id);
    }

    this.set_profile_logged = function(profile) {
      profile_logged = profile;
    }
  })

  .run(function ($log) {
    $log.debug('services running!');
  });
