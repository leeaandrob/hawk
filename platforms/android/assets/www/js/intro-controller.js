.controller('IntroCtrl', function($scope, $log, $state, AuthService) {
    $log.debug('intro controller');
    $scope.intro = true;

    function onAuthenticated(me) {
        $log.debug('intro: authenticated');
        AuthService
            .auth(me.identities[0].provider, me)
            .success(function(data, status) {
                $state.go('app.register');
            });
    }

    function onNotAuthenticated() {
        $log.debug('intro: not authenticated');
        $scope.login = AuthService.login;
    }
    if (window.Hull.currentUser && window.Hull.currentUser()) {
        onAuthenticated(window.Hull.currentUser());
    } else {
        window.Hull.on('hull.init', function(hull, me) {
            if (me) {
                onAuthenticated(me);
            } else {
                onNotAuthenticated();
            }
        });

        window.Hull.on('hull.auth.login', function(me) {
            if (me) {
                onAuthenticated(me);
            } else {
                onNotAuthenticated();
            }
        });
    }
})
