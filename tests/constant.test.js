describe('App constant', function () {
  beforeEach(module('starter.constant'));

  it('should be equal localhost:3000 api address', inject(function (DEV) {
    expect(DEV).toEqual(window.location.host.replace(/\:[0-9]+/, ':3000'));
  }));

  it('should be equal ip from production server api address', inject(function (PROD) {
    expect(PROD).toEqual('162.209.79.99');
  }));
});
